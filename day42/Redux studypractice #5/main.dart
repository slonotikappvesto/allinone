import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:reduxsecpr/data_view_model.dart';
import 'package:reduxsecpr/store/app/app_state.dart';

void main() {
  Store store = Store<AppState>(AppState.getAppReducer,
      initialState: AppState.initial(), middleware: []);
  runApp(MyApp(store: store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  const MyApp({@required this.store, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: DataPage(),
      ),
    );
  }
}

class DataPage extends StatefulWidget {
  const DataPage({Key key}) : super(key: key);

  @override
  _DataPageState createState() => _DataPageState();
}

class _DataPageState extends State<DataPage> {
  int page = 0;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, DataPageViewModel>(
      converter: DataPageViewModel.fromStore,
      onInitialBuild: (DataPageViewModel dataViewModel) =>
          dataViewModel.getData(),
      builder: (BuildContext context, DataPageViewModel dataViewModel) {
        String getData() {
          if (page == 0) {
            return dataViewModel.data.toString();
          } else if (page == 1) {
            return (dataViewModel.data / 2).toString();
          } else if (page == 2) {
            return (dataViewModel.data * 10).toString();
          }
        }

        return MaterialApp(
          home: Scaffold(
            drawer: Drawer(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      dataViewModel.inc();
                    },
                    child: Text('Plus'),
                  ),
                  InkWell(
                    onTap: () {
                      dataViewModel.dec();
                    },
                    child: Text('Minus'),
                  ),
                  InkWell(
                    onTap: () {
                      page = 0;
                      setState(() {});
                    },
                    child: Text('Page 1'),
                  ),
                  InkWell(
                    onTap: () {
                      page = 1;
                      setState(() {});
                    },
                    child: Text('Page 2'),
                  ),
                  InkWell(
                    onTap: () {
                      page = 2;
                      setState(() {});
                    },
                    child: Text('Page 3'),
                  ),
                ],
              ),
            ),
            appBar: AppBar(
              title: Text('Data'),
              centerTitle: true,
            ),
            body: Container(
              child: Center(
                child: Text(
                  getData() ?? 'Error',
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
