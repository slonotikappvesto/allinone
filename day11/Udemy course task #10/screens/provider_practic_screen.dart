import 'package:anim/provider/text.dart';
import 'package:anim/widgets/vButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProviderPractic extends StatefulWidget {
  @override
  _ProviderPracticState createState() => _ProviderPracticState();
}

class _ProviderPracticState extends State<ProviderPractic> {
  @override
  Widget build(BuildContext context) {
    final textData = Provider.of<TextData>(context);
    final title = textData.textTitle;
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(title),
          SizedBox(
            height: 25,
          ),
          VladButton(
            'First',
          ),
          SizedBox(
            height: 25,
          ),
          VladButton(
            'Second',
          ),
          SizedBox(
            height: 25,
          ),
          VladButton(
            'Wow it works',
          ),
          SizedBox(
            height: 25,
          ),
          VladButton(
            'Best mentor',
          ),
        ],
      ),
    );
  }
}
