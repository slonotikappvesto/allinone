import 'package:flutter/cupertino.dart';

class TextData with ChangeNotifier {
  String _titleText = 'Noooo, faste, faster...';

  String get textTitle {
    return _titleText;
  }

  void updateText(newTitle) {
    _titleText = newTitle;
    notifyListeners();
  }
}
