import 'package:anim/provider/text.dart';
import 'package:anim/screens/provider_practic_screen.dart';
import 'package:anim/services/main_layout.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(HomePage());
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => TextData(),
      child: MaterialApp(
        title: 'Provider',
        home: MainLayout(
          appBar: AppBar(
            title: Text('Provider'),
          ),
          child: ProviderPractic(),
        ),
      ),
    );
  }
}
