import 'package:anim/provider/text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProviderPractic extends StatefulWidget {
  @override
  _ProviderPracticState createState() => _ProviderPracticState();
}

class _ProviderPracticState extends State<ProviderPractic>
    with TickerProviderStateMixin {
  final items = List<int>.generate(100, (i) => i);

  AnimationController _controller;
  Animation<double> _size;
  bool isAnim = true;
  var icon = Icons.arrow_downward;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 1000),
        reverseDuration: Duration(milliseconds: 1000));
    _size = Tween<double>(begin: 0, end: 500).animate(_controller);
    _controller.addListener(() {});
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void tapAnimation() {
    if (isAnim) {
      isAnim = !isAnim;
      setState(() {
        icon = Icons.close;
      });

      _controller.forward();
    } else {
      isAnim = !isAnim;
      setState(() {
        icon = Icons.arrow_downward;
      });
      _controller.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    final textData = Provider.of<TextData>(context);
    final title = textData.textTitle;
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(title),
          SizedBox(
            height: 25,
          ),
          InkWell(
            onTap: tapAnimation,
            child: Container(
              height: 60,
              width: 120,
              color: Colors.green[300],
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'List',
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    icon,
                    size: 18,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 25,
          ),
          AnimatedBuilder(
              animation: _controller,
              builder: (BuildContext context, Widget _) {
                return Container(
                  width: 270,
                  height: _size.value,
                  child: ListView.builder(
                      itemCount: items.length,
                      itemBuilder: (ctx, index) {
                        return InkWell(
                          onTap: () {
                            Provider.of<TextData>(context)
                                .updateText('Clicked: Item $index');
                            tapAnimation();
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: Container(
                              height: 50,
                              margin: EdgeInsets.all(3),
                              alignment: Alignment.center,
                              color: Colors.blue[300],
                              child: Text(
                                'Item $index',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                );
              }),
        ],
      ),
    );
  }
}
