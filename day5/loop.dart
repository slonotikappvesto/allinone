void main() {
  print('\"For\"');

  for (int ctr = 1; ctr <= 5; ctr++) {
    print(ctr);
  }

  print('\"For-in\"');

  var obj = [
    9,
    3,
    17,
    1,
    6,
  ];

  for (var prop in obj) {
    print(prop);
  }

  print('\"forEach\"');
  int sum = 0;
  obj.forEach((elm) {
    sum += elm;
  });
  print(sum);

  print('\"While\"');
  var bool = true;
  while(bool) {
    if (sum <= 32) {
      bool = false;
      print('False');
    } else {
      print('---');
      sum--;
    }
  }

  print('\"Do while\"');
  do {
    print('First iteration');
  } while (bool);


  print('\"Map\"');
  List<String> names = ['Vlad', 'Ihor', 'Sofia', 'Artem'];
  var lengthNames = names.map((name) => {
    name.length
  });

  print(lengthNames);

}


