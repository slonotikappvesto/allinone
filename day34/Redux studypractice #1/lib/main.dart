import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:reduxsimple/redux/app_state.dart';

import 'redux/actions.dart';
import 'redux/reducers.dart';

void main() {
  final Store<AppState> store = Store(reducer,
      initialState: AppState(
          counter: 0, text: "Waiting...", colorAppBar: Colors.grey[800]));
  runApp(StoreProvider(
    store: store,
    child: MaterialApp(
      home: Counter(),
    ),
  ));
}

class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  bool _isSwitch = false;

  @override
  Widget build(BuildContext context) {
    final Store<AppState> store = StoreProvider.of<AppState>(context);
    String inputText = "";
    return Scaffold(
      appBar: PreferredSize(
        child: StoreConnector<AppState, AppState>(
          converter: (store) => store.state,
          builder: (context, vm) => AppBar(
            backgroundColor: vm.colorAppBar,
          ),
        ),
        preferredSize: const Size.fromHeight(100),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => store.dispatch(AddAction()),
        child: Icon(
          Icons.fire_hydrant,
        ),
      ),
      drawer: Drawer(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CupertinoSwitch(
              value: _isSwitch,
              onChanged: (val) {
                setState(() {
                  _isSwitch = val;
                  val
                      ? store.dispatch(
                          SetAppBarColor(color: Colors.orange),
                        )
                      : store.dispatch(
                          SetAppBarColor(color: Colors.grey[850]),
                        );
                });
              },
            ),
            TextButton(
                onPressed: () {
                  store.dispatch(ClearAction());
                },
                child: Text("Press to reset data")),
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 200,
              child: TextField(
                onChanged: (value) => inputText = value,
              ),
            ),
            SizedBox(height: 20),
            TextButton(
                onPressed: () => store.dispatch(SetTextAction(text: inputText)),
                child: Text("Press to input data")),
            SizedBox(height: 20),
            StoreConnector<AppState, AppState>(
                converter: (store) => store.state,
                builder: (context, vm) => Text(vm.text)),
            SizedBox(height: 120),
            SizedBox(height: 120),
            StoreConnector<AppState, AppState>(
              converter: (store) => store.state,
              builder: (context, state) => Text(
                state.counter.toString(),
                style: TextStyle(fontSize: 35),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
