import 'package:flutter/cupertino.dart';

class AddAction {}

class SetTextAction {
  final String text;

  SetTextAction({
    @required this.text,
  });
}

class ClearAction {}

class SetAppBarColor {
  final Color color;

  SetAppBarColor({@required this.color});
}
