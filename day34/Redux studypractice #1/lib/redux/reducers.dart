import 'package:reduxsimple/redux/app_state.dart';

import 'actions.dart';

AppState reducer(AppState state, dynamic action) {
  if (action is AddAction) {
    return AppState(
        counter: state.counter + 1,
        text: state.text,
        colorAppBar: state.colorAppBar);
  } else if (action is SetTextAction) {
    return AppState(
        counter: state.counter,
        text: action.text,
        colorAppBar: state.colorAppBar);
  } else if (action is ClearAction) {
    return AppState(counter: 0, text: ' ', colorAppBar: state.colorAppBar);
  } else if (action is SetAppBarColor) {
    return AppState(counter: 0, text: ' ', colorAppBar: action.color);
  } else {
    return state;
  }
}
