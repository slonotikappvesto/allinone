import 'package:flutter/cupertino.dart';
import 'package:fpractday26/models/product.dart';

class Products extends ChangeNotifier {
  List<Product> _products = [];

  List<Product> get productsList {
    return _products;
  }
}
