class Product {
  final int id;
  final String brand;
  final String name;
  final String price;
  final String photoUrl;
  final String description;
  final double rating;

  Product({
    this.id,
    this.brand,
    this.name,
    this.price,
    this.photoUrl,
    this.description,
    this.rating,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      id: json['id'] ?? 11,
      brand: json['brand'] ?? '',
      name: json['name'] ?? '',
      price: json['price'] ?? '',
      photoUrl: json['image_link'] ?? '',
      description: json['description'] ?? '',
      rating: json['rating'] ?? 2.2,
    );
  }
}
