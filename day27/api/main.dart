import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fpractday26/models/product.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Shop Demo';

    return MaterialApp(
      title: appTitle,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Product> products = [];
  bool isLoaded = false;

  productsData() async {
    var response = await http.get(
        'http://makeup-api.herokuapp.com/api/v1/products.json?brand=maybelline');
    List<dynamic> productsLists = jsonDecode(response.body);
    for (var item in productsLists) {
      products.add(Product.fromJson(item));
    }
    setState(() {
      isLoaded = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    productsData();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Shop Olala',
          style: TextStyle(
            color: Colors.red[300],
          ),
        ),
        backgroundColor: Colors.blue[100],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 24.0),
        child: Center(
          child: isLoaded
              ? ProductsList(
                  products: products,
                )
              : CircularProgressIndicator(),
        ),
      ),
    );
  }
}

class ProductsList extends StatelessWidget {
  final List<Product> products;

  ProductsList({Key key, this.products}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10),
      itemCount: products.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DetailPage(
                        product: products[index],
                      )),
            );
          },
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          splashColor: Colors.transparent,
          child: Container(
            width: double.infinity,
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
            margin: const EdgeInsets.symmetric(horizontal: 3.0),
            decoration: BoxDecoration(
                color: Colors.red[200],
                shape: BoxShape.rectangle,
                borderRadius: const BorderRadius.all(Radius.circular(15.0))),
            child: GridTile(
              header: Text(
                '${products[index].name}',
                overflow: TextOverflow.clip,
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              footer: Text('\$${products[index].price}'),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Row(
                    children: [
                      Spacer(),
                      Icon(
                        Icons.star,
                        color: Colors.yellow[100],
                        size: 16,
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text('${products[index].rating} / 5'),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class DetailPage extends StatelessWidget {
  final Product product;

  const DetailPage({Key key, this.product}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            '${product.name}',
            style: TextStyle(
              color: Colors.red[300],
            ),
          ),
          backgroundColor: Colors.blue[100],
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.network(product.photoUrl),
                  SizedBox(
                    height: 32,
                  ),
                  Text(
                    '\$${product.price}',
                    style: TextStyle(color: Colors.red[300], fontSize: 26),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.star,
                        size: 32,
                        color: Colors.yellow,
                      ),
                      Icon(
                        Icons.star,
                        size: 32,
                        color: Colors.yellow,
                      ),
                      Icon(
                        Icons.star,
                        size: 32,
                        color: Colors.yellow,
                      ),
                      Icon(
                        Icons.star,
                        size: 32,
                        color: Colors.yellow,
                      ),
                      Icon(
                        Icons.star,
                        size: 32,
                        color: Colors.yellow,
                      ),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 14.0),
                    margin: const EdgeInsets.only(top: 32),
                    child: Text(
                      '${product.description}',
                      style: TextStyle(color: Colors.black54),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
