import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'wotw10',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double targetValue = 24.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: Colors.amber,
            title: Text('FASTER FASTER FASET'),
            expandedHeight: 30,
            collapsedHeight: 150,
          ),
          SliverAppBar(
            backgroundColor: Colors.green,
            title: Text('Have a nice day MANNNN'),
            floating: true,
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Dismissible(
                  key: ValueKey('$index dasd'),
                  background: Container(
                    color: Colors.redAccent,
                  ),
                  child: Card(
                    margin: EdgeInsets.all(15),
                    child: Container(
                      color: Colors.blue[100 * (index % 9 + 1)],
                      height: 80,
                      alignment: Alignment.center,
                      child: Text(
                        "Item $index",
                        style: TextStyle(fontSize: 30),
                      ),
                    ),
                  ),
                );
              },
              childCount: 10,
            ),
          ),
          TweenAnimationBuilder<double>(
            tween: Tween<double>(begin: 0, end: targetValue),
            duration: const Duration(seconds: 1),
            builder: (BuildContext context, double size, Widget child) {
              return IconButton(
                iconSize: size,
                color: Colors.blue,
                icon: child,
                onPressed: () {
                  setState(() {
                    targetValue = targetValue == 24.0 ? 48.0 : 24.0;
                  });
                },
              );
            },
            child: const Icon(Icons.aspect_ratio),
          ),
        ],
      ),
    );
  }
}
