import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PracticAll",
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Drawer'),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            Text('Hi'),
          ],
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  height: mediaQuery.size.height * 0.1,
                  width: mediaQuery.size.width * 0.5,
                  color: Colors.red,
                  child: Opacity(
                    opacity: 0.75,
                    child: Text(
                      'Opacity',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: mediaQuery.size.height * 0.025,
                ),
                Text("ClipOval"),
                SizedBox(
                  height: mediaQuery.size.height * 0.025,
                ),
                Container(
                  height: mediaQuery.size.width * 0.4,
                  width: mediaQuery.size.width * 0.4,
                  child: ClipOval(
                    child: Image.network(
                        'https://pbs.twimg.com/media/EyNmsawXIAIz1-h.jpg'),
                  ),
                ),
                SizedBox(
                  height: mediaQuery.size.height * 0.025,
                ),
                Text("Rotate"),
                SizedBox(
                  height: mediaQuery.size.height * 0.025,
                ),
                Transform.rotate(
                  angle: pi / 4,
                  child: Icon(
                    Icons.whatshot,
                    size: 40,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
