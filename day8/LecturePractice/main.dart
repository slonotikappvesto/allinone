import 'package:app/main_layout.dart';
import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PracticAll",
      home: MainLayout(
        appBar: AppBar(
          title: Text('Its me'),
        ),
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Center(
      child: InkWell(
        onTap: () {
          print('Tap');
        },
        child: Container(
          alignment: Alignment.center,
          height: mediaQuery.size.height * 0.15,
          width: mediaQuery.size.width * 0.8,
          color: Colors.red,
          child: Text(
            'dynamic text and button',
            style: TextStyle(
              color: Colors.white,
              fontSize: MediaQuery.of(context).size.width * 0.06,
            ),
          ),
        ),
      ),
    );
  }
}
