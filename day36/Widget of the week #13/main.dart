import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'wotw13',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

Widget block = Container(
  width: 50,
  height: 50,
  color: Colors.blue,
);

class _HomePageState extends State<HomePage> {
  double targetValue = 24.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(28.0),
              child: Column(
                children: [
                  Text('Semantics'),
                  Semantics(
                    enabled: true,
                    child: Tooltip(
                      message: 'It\'s a start',
                      child: Icon(
                        Icons.star,
                        color: Colors.yellow,
                        size: 48,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Text('ClipPath'),
                  SizedBox(
                    height: 16,
                  ),
                  ClipPath(
                    clipper: CustomClipPath(),
                    child: Container(
                      width: double.infinity,
                      height: 120,
                      color: Colors.green,
                    ),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Text('Wrap'),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    color: Colors.yellow,
                    width: double.infinity,
                    padding: const EdgeInsets.all(10.0),
                    height: 150,
                    child: Wrap(
                      alignment: WrapAlignment.start,
                      spacing: 16.65,
                      runSpacing: 20.0,
                      children: [
                        block,
                        block,
                        block,
                        block,
                        block,
                        block,
                        block,
                        block,
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Text('AbsorbPointer'),
                  SizedBox(
                    height: 16,
                  ),
                  AbsorbPointer(
                    absorbing: true,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RaisedButton(
                          onPressed: () {},
                          color: Colors.blue[200],
                        ),
                        RaisedButton(
                          onPressed: () {},
                          color: Colors.blue[200],
                        ),
                        RaisedButton(
                          onPressed: () {},
                          color: Colors.blue[200],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Text('AboutDialog'),
                  SizedBox(
                    height: 16,
                  ),
                  RaisedButton(
                    onPressed: () {
                      showAboutDialog(
                        context: context,
                        applicationIcon: Icon(Icons.star),
                        applicationVersion: '1.0.1',
                        applicationLegalese: 'Wow wow.',
                      );
                    },
                    color: Colors.red[400],
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

class CustomClipPath extends CustomClipper<Path> {
  var radius = 10.0;
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height);
    path.arcToPoint(Offset(size.width, size.height),
        radius: Radius.elliptical(30, 10));
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
