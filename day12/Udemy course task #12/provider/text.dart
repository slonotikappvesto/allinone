import 'package:flutter/cupertino.dart';

class TextData with ChangeNotifier {
  String _titleText = 'Noooo, faste, faster...';

  final items = List<int>.generate(100, (i) => i);

  var _count = 3;

  String get textTitle {
    return _titleText;
  }

  int get count {
    return _count;
  }

  void plusCount() {
    _count++;
    notifyListeners();
  }

  void minusCount() {
    _count--;
    notifyListeners();
  }

  void updateText(newTitle) {
    _titleText = newTitle;
    notifyListeners();
  }
}
