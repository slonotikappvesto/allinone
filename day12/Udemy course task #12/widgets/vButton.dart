import 'package:anim/provider/text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class VladButton extends StatelessWidget {
  final String title;

  VladButton(this.title);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Provider.of<TextData>(context).updateText(title),
      child: Container(
        alignment: Alignment.center,
        height: 60,
        width: 130,
        color: Colors.greenAccent,
        child: Text(title),
      ),
    );
  }
}
