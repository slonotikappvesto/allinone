import 'package:anim/provider/text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProviderPractic extends StatefulWidget {
  @override
  _ProviderPracticState createState() => _ProviderPracticState();
}

class _ProviderPracticState extends State<ProviderPractic> {
  @override
  Widget build(BuildContext context) {
    final textData = Provider.of<TextData>(context);
    final items = textData.items;
    final title = textData.textTitle;
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            height: 65,
          ),
          Text(title),
          InkWell(
            child: Container(
              height: 60,
              width: 120,
              color: Colors.green[300],
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'List',
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.star,
                    size: 18,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: 370,
            height: 300,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: textData.count,
                itemBuilder: (ctx, index) {
                  return InkWell(
                    onTap: () {
                      Provider.of<TextData>(context)
                          .updateText('Clicked: Item $index');
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: Container(
                        height: 50,
                        width: 120,
                        margin: EdgeInsets.all(3),
                        alignment: Alignment.center,
                        color: Colors.blue[300],
                        child: Text(
                          'Item $index',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  );
                }),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                onTap: () => textData.minusCount(),
                child: Container(
                  height: 60,
                  width: 120,
                  color: Colors.green[300],
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Minus',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.remove,
                        size: 18,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 30,
              ),
              InkWell(
                onTap: () => textData.plusCount(),
                child: Container(
                  height: 60,
                  width: 120,
                  color: Colors.green[300],
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Plus',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.add,
                        size: 18,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
