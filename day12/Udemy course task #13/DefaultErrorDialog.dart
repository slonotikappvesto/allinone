import 'package:flutter/material.dart';
import 'package:silberAppBar/iDialog.dart';

class DefaultErrorDialog extends StatelessWidget implements IDialog {
  @override
  final Widget widgetDialogs = AlertDialog(
    title: Text('Pls faster faster...?'),
    content: Text(
      'You are fast?',
    ),
    actions: [
      FlatButton(
        onPressed: () {},
        child: Text('Yes, I am very fast'),
      ),
    ],
  );

  @override
  void show(BuildContext context) {
    showDialog(
      context: context,
      builder: (ctx) => widgetDialogs,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DHelper'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Faster faster...'),
            SizedBox(
              height: 50,
            ),
            InkWell(
              onTap: () {
                show(context);
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.redAccent,
                height: 50,
                width: 140,
                child: Text('Error'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
