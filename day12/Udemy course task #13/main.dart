import 'package:flutter/material.dart';
import 'package:silberAppBar/DefaultErrorDialog.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dialog Helper',
      home: DefaultErrorDialog(),
    );
  }
}
