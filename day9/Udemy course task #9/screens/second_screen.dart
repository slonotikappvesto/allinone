import 'package:app/services/main_layout.dart';
import 'package:flutter/material.dart';

class SecondtScreen extends StatelessWidget {
  final String lastPage;

  const SecondtScreen(this.lastPage);

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('2'),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Current page: Second Page'),
            Text('Last page: $lastPage'),
            SizedBox(
              height: 50,
            ),
            InkWell(
              onTap: () {
                Navigator.of(context)
                    .pushNamed('/ohnow', arguments: 'Second Page');
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.redAccent,
                height: 70,
                width: 150,
                child: Text(
                  'New Page',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
