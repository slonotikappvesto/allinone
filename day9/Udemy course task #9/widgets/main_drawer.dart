import 'package:app/services/router_generator.dart';
import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  Widget buildButton(String name, Function tapHandler) {
    return InkWell(
      onTap: tapHandler,
      child: Container(
        alignment: Alignment.center,
        height: 70,
        width: 200,
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 5,
        ),
        decoration: BoxDecoration(
          color: Colors.blueAccent,
          boxShadow: [BoxShadow(blurRadius: 16.0, offset: Offset(4.0, 4.0))],
        ),
        child: Text(
          name,
          style: TextStyle(
            color: Colors.white,
            fontSize: 22,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          SizedBox(
            height: 100,
          ),
          buildButton('First', () {
            Navigator.of(context)
                .pushNamed(RouteGenerater.ROUTE_FIRST, arguments: 'Main Page');
          }),
          buildButton('Second', () {
            Navigator.of(context)
                .pushNamed(RouteGenerater.ROUTE_SECOND, arguments: 'Main Page');
          }),
        ],
      ),
    );
  }
}
