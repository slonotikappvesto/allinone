import 'package:flutter/material.dart';

class MainLayout extends StatelessWidget {
  final Widget child;
  final PreferredSizeWidget appBar;

  const MainLayout({Key key, this.appBar, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: child,
      ),
    );
  }
}
