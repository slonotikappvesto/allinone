import 'package:app/services/main_layout.dart';
import 'package:app/services/router_generator.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PracticAll",
      home: MainLayout(
        appBar: AppBar(
          title: Text('Udemy 8'),
        ),
        child: MyHomePage(),
      ),
      onGenerateRoute: RouteGenerater.generateRoute,
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              Navigator.of(context).pushNamed(RouteGenerater.ROUTE_FIRST,
                  arguments: 'Main Page');
            },
            child: Container(
              alignment: Alignment.center,
              height: mediaQuery.size.height * 0.15,
              width: mediaQuery.size.width * 0.8,
              color: Colors.red,
              child: Text(
                'First Page',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: MediaQuery.of(context).size.width * 0.06,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () {
              Navigator.of(context).pushNamed(RouteGenerater.ROUTE_SECOND,
                  arguments: 'Main Page');
            },
            child: Container(
              alignment: Alignment.center,
              height: mediaQuery.size.height * 0.15,
              width: mediaQuery.size.width * 0.8,
              color: Colors.red,
              child: Text(
                'Second',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: MediaQuery.of(context).size.width * 0.06,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
