import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Wow",
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _titleController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('LP'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              SizedBox(
                width: 120,
                height: 100,
                child: RaisedButton(
                  color: Colors.green,
                  onPressed: () {},
                  child: Text(
                    "SizeBox",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(20),
                width: mediaQuery.size.width,
                height: mediaQuery.size.height * 0.3,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.all(10),
                  children: [
                    RaisedButton(
                      color: Colors.greenAccent,
                      onPressed: () {},
                      child: Text(
                        "1",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    RaisedButton(
                      color: Colors.green,
                      onPressed: () {},
                      child: Text(
                        "2",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    RaisedButton(
                      color: Colors.orangeAccent,
                      onPressed: () {},
                      child: Text(
                        "3",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    RaisedButton(
                      color: Colors.orange,
                      onPressed: () {},
                      child: Text(
                        "4",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    RaisedButton(
                      color: Colors.redAccent,
                      onPressed: () {},
                      child: Text(
                        "5",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    RaisedButton(
                      color: Colors.red,
                      onPressed: () {},
                      child: Text(
                        "6",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ),
              ListTile(
                leading: Icon(Icons.account_balance),
                title: Text("ListTitle"),
                subtitle: Text("Wow"),
                trailing: Icon(Icons.access_alarm),
              ),
              SelectableText(
                "selectable text",
                showCursor: true,
                cursorWidth: 5,
                cursorColor: Colors.yellow,
              )
            ],
          ),
        ),
      ),
    );
  }
}
