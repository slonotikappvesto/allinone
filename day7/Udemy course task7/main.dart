import 'package:app/models/transaction.dart';
import 'package:app/utils/dateformater.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Wow",
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<Transaction> _userTransactions = [
    Transaction(
      id: 't1',
      title: 'New Car',
      amount: 699.99,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't2',
      title: 'Fruits',
      amount: 9.42,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't3',
      title: 'Surprise',
      amount: 69.99,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't4',
      title: 'New Car',
      amount: 699.99,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't5',
      title: 'New Car',
      amount: 699.99,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't6',
      title: 'Fruits',
      amount: 9.42,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't7',
      title: 'Surprise',
      amount: 69.99,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't8',
      title: 'New Car',
      amount: 699.99,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't9',
      title: 'New Car',
      amount: 699.99,
      date: DateTime.now(),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('LP'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: mediaQuery.size.height * 0.4,
              width: mediaQuery.size.width,
              child: ListView.builder(
                itemCount: _userTransactions.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(_userTransactions[index].title),
                    subtitle: Text(_userTransactions[index].date.toString()),
                    trailing: Icon(Icons.delete),
                    leading: Icon(Icons.account_balance),
                  );
                },
              ),
            ),
            SizedBox(
              height: mediaQuery.size.height * 0.1,
            ),
            Container(
              height: mediaQuery.size.height * 0.4,
              width: mediaQuery.size.width,
              child: GridView.builder(
                itemCount: _userTransactions.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3),
                itemBuilder: (context, index) {
                  return Text(_userTransactions[index].title);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
