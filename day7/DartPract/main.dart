import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Wow",
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<List<int>> numbers =
      new List.generate(11, (list) => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

  int counter = -1;
  int counterRow = -1;
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    print(numbers);

    return Scaffold(
      appBar: AppBar(
        title: Text('LP'),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                ...numbers.map((list) {
                  counter++;
                  counterRow = -1;
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ...list.map((number) {
                        counterRow++;
                        if (counter == 0) {
                          return Container(
                            alignment: Alignment.center,
                            height: mediaQuery.size.height * 0.04,
                            width: mediaQuery.size.height * 0.04,
                            color: Colors.greenAccent,
                            child: Text(number.toString()),
                          );
                        } else {
                          if (counterRow == 0) {
                            return Container(
                              alignment: Alignment.center,
                              height: mediaQuery.size.height * 0.04,
                              width: mediaQuery.size.height * 0.04,
                              color: Colors.greenAccent,
                              child: Text((counter).toString()),
                            );
                          } else {
                            if (number == counter) {
                              return Container(
                                alignment: Alignment.center,
                                height: mediaQuery.size.height * 0.04,
                                width: mediaQuery.size.height * 0.04,
                                color: Colors.yellow[300],
                                child: Text((number * counter).toString()),
                              );
                            } else if (number > counter) {
                              return Container(
                                alignment: Alignment.center,
                                height: mediaQuery.size.height * 0.04,
                                width: mediaQuery.size.height * 0.04,
                                color: Colors.white12,
                                child: Text((number * counter).toString()),
                              );
                            } else {
                              return Container(
                                alignment: Alignment.center,
                                height: mediaQuery.size.height * 0.04,
                                width: mediaQuery.size.height * 0.04,
                                color: Colors.orange[300],
                                child: Text((number * counter).toString()),
                              );
                            }
                          }
                        }
                      })
                    ],
                  );
                })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
