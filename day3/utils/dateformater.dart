import 'package:intl/intl.dart';

class DateFormatUtil {

  static String formatDateToString(DateTime date) {
    return DateFormat.yMd().add_jm().format(date);
  }
}