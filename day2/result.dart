import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetQuiz;

  Result(this.resultScore, this.resetQuiz);

  String get resultPhrase {
    String resultText;
    if (resultScore <= 8) {
      resultText = 'Awesome';
    } else if (resultScore <= 8) {
      resultText = 'Pretty';
    } else if (resultScore <= 12) {
      resultText = 'Strange?';
    } else {
      resultText = 'So bad :(';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Text(
            resultPhrase,
            style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
          ),
        ),
        Image.network(
            'https://media-exp1.licdn.com/dms/image/C4D1BAQFmCR8YppicQQ/company-background_10000/0/1595835654059?e=2159024400&v=beta&t=oFU8pv_pXDJFTUAe2sFQCdOh6N85fYPV-9vhWNJZaF4'),
        FlatButton(
          onPressed: resetQuiz,
          textColor: Colors.blue,
          child: Text('Restart'),
        )
      ],
    );
  }
}
