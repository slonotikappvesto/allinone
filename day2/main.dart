import 'package:flutter/material.dart';
import 'package:guide_test/result.dart';

import './result.dart';
import './quiz.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  static const _questions = [
    {
      'questionText': 'What\'s you like?',
      'answers': [
        {'text': 'Apple', 'score': 10},
        {'text': 'Banana', 'score': 5},
        {'text': 'Kiwi', 'score': 3}
      ]
    },
    {
      'questionText': 'What\'s you like?',
      'answers': [
        {'text': 'Apple', 'score': 6},
        {'text': 'Banana', 'score': 5},
        {'text': 'Kiwi1', 'score': 3}
      ]
    },
    {
      'questionText': 'What\'s you like?',
      'answers': [
        {'text': 'Apple', 'score': 8},
        {'text': 'Banana', 'score': 3},
        {'text': 'Kiwi2', 'score': 2}
      ]
    },
  ];

  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestions(int score) {
    _totalScore += score;
    setState(() {
      _questionIndex++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('App Guide'),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                answerQuestion: _answerQuestions,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
