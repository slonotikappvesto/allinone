import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'wotw13',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double targetValue = 24.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(28.0),
            child: Column(
              children: [
                Text('Tooltip'),
                Tooltip(
                  message: 'It\'s a start',
                  child: Icon(
                    Icons.star,
                    color: Colors.yellow,
                    size: 48,
                  ),
                ),
                SizedBox(
                  height: 32,
                ),
                Text('Placeholder'),
                Placeholder(
                  fallbackHeight: 200,
                ),
                SizedBox(
                  height: 32,
                ),
                FittedBox(
                  child: Text(
                    'asdasasadadasda FittedBox d asd as',
                    style: TextStyle(fontSize: 45),
                  ),
                ),
                SizedBox(
                  height: 32,
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 200),
                  child: Text(
                    'Constrained Box',
                    style: TextStyle(fontSize: 37),
                  ),
                ),
                SizedBox(
                  height: 32,
                ),
                Text(
                  'LimitedBox',
                ),
                Expanded(
                  child: ListView(
                    children: [
                      LimitedBox(
                        maxHeight: 50,
                        child: Container(
                          color: Colors.greenAccent,
                        ),
                      ),
                      LimitedBox(
                        maxHeight: 50,
                        child: Container(
                          color: Colors.orangeAccent,
                        ),
                      ),
                      LimitedBox(
                        maxHeight: 50,
                        child: Container(
                          color: Colors.redAccent,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
