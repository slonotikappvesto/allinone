import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Day16Gen',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  Animation<double> _size;
  Animation<double> _shadow;

  AnimationController _controller;

  bool isAnim = true;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 1000),
        reverseDuration: Duration(milliseconds: 1000));

    _size = Tween<double>(begin: 70, end: 150).animate(_controller);

    _shadow = Tween<double>(begin: 0, end: 20).animate(_controller);

    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void tapAnimation() {
    if (isAnim) {
      isAnim = !isAnim;
      _controller.forward();
    } else {
      isAnim = !isAnim;
      _controller.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      primary: true,
      appBar: CustomAppBar(
        child: Text('Wow'),
        heightStatusBar: MediaQuery.of(context).padding.top,
        height: _size.value,
        shadow: _shadow.value,
      ),
      body: SafeArea(
        child: Center(
          child: InkWell(
            onTap: tapAnimation,
            child: Container(
              alignment: Alignment.center,
              height: 60,
              width: 120,
              color: Colors.greenAccent,
              child: Text('Faster'),
            ),
          ),
        ),
      ),
    );
  }
}

class CustomAppBar extends PreferredSize {
  final Widget child;
  final double height;
  final double heightStatusBar;
  final double shadow;

  CustomAppBar(
      {@required this.child,
      @required this.heightStatusBar,
      @required this.shadow,
      this.height = kToolbarHeight});

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: heightStatusBar,
          color: Colors.orange,
        ),
        Container(
          height: preferredSize.height,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                blurRadius: shadow,
                offset: Offset(0.0, shadow),
              )
            ],
            color: Colors.orange,
          ),
          child: child,
        ),
      ],
    );
  }
}
