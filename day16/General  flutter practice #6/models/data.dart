import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DataModel {
  final DateTime date;
  final TimeOfDay time;
  final double value;
  final String gender;

  DataModel({
    @required this.date,
    @required this.time,
    @required this.value,
    @required this.gender,
  });

  @override
  String toString() {
    return 'Selected data: Date: ${DateFormat.yMMMd().format(date)} | Time: ${time.toString()} | Value $value | Gender: $gender';
  }
}
