import 'package:day16gen/models/data.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Day16Gen',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DataModel _modelData;
  DateTime _selectedDate;
  TimeOfDay _selectedTime;
  double _currentSliderValue = 0;
  String _dropdownValue = 'Male';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gen p 16'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(2021, 1, 1),
                        lastDate: DateTime(2022, 1, 1))
                    .then((pickedData) {
                  if (pickedData == null) {
                    return;
                  }
                  setState(() {
                    _selectedDate = pickedData;
                  });
                });
              },
              child: Container(
                alignment: Alignment.center,
                width: 120,
                height: 60,
                color: Colors.greenAccent,
                child: Text('DatePicker'),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              _selectedDate == null
                  ? 'No date chosen'
                  : 'Picked date: ${DateFormat.yMMMd().format(_selectedDate)}',
            ),
            SizedBox(
              height: 30,
            ),
            InkWell(
              onTap: () {
                showTimePicker(context: context, initialTime: TimeOfDay.now())
                    .then((pickedTime) {
                  if (pickedTime == null) return;
                  setState(() {
                    _selectedTime = pickedTime;
                  });
                });
              },
              child: Container(
                alignment: Alignment.center,
                width: 120,
                height: 60,
                color: Colors.greenAccent,
                child: Text('TimePicker'),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              _selectedTime == null
                  ? 'No time chosen'
                  : 'Picked time: ${_selectedTime.format(context)}',
            ),
            SizedBox(
              height: 30,
            ),
            Slider(
              value: _currentSliderValue,
              min: 0,
              max: 1,
              divisions: 100,
              label: _currentSliderValue.toStringAsFixed(2),
              onChanged: (double value) {
                setState(() {
                  _currentSliderValue = value;
                });
              },
            ),
            Text('Picked value: ${_currentSliderValue.toStringAsFixed(2)}'),
            SizedBox(
              height: 30,
            ),
            DropdownButton<String>(
              value: _dropdownValue,
              icon: const Icon(Icons.arrow_downward),
              iconSize: 24,
              elevation: 16,
              style: const TextStyle(color: Colors.deepPurple),
              underline: Container(
                height: 2,
                color: Colors.deepPurpleAccent,
              ),
              onChanged: (String newValue) {
                setState(() {
                  _dropdownValue = newValue;
                });
              },
              items: <String>['Male', 'Female']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
            SizedBox(
              height: 50,
            ),
            InkWell(
              onTap: () {
                setState(() {
                  _modelData = DataModel(
                      date: _selectedDate,
                      time: _selectedTime,
                      value: _currentSliderValue,
                      gender: _dropdownValue);
                });
              },
              child: Container(
                alignment: Alignment.center,
                width: 120,
                height: 60,
                color: Colors.orangeAccent,
                child: Text('Save model'),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(22.0),
              child: Text(_modelData == null ? '' : _modelData.toString()),
            ),
          ],
        ),
      ),
    );
  }
}
