import 'package:app/screens/error_screen.dart';
import 'package:app/screens/first_screen.dart';
import 'package:app/screens/second_screen.dart';
import 'package:flutter/material.dart';

class RouteGenerater {
  static const ROUTE_FIRST = "/first";
  static const ROUTE_SECOND = "/second";
  static const ROUTE_ERROR = "/error";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ROUTE_FIRST:
        final page = FirstScreen(settings.arguments);
        return MaterialPageRoute(builder: (context) => page);

      case ROUTE_SECOND:
        final page = SecondtScreen(settings.arguments);
        return MaterialPageRoute(builder: (context) => page);

      default:
        return MaterialPageRoute(
            builder: (context) => ErrorScreen(settings.arguments));
    }
  }
}
