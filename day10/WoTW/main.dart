import 'package:app/services/main_layout.dart';
import 'package:app/services/router_generator.dart';
import 'package:app/widgets/main_drawer.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PracticAll",
      home: MainLayout(
        appBar: AppBar(
          title: Text('WoTW'),
        ),
        child: MyHomePage(),
        drawer: MyDrawer(),
      ),
      onGenerateRoute: RouteGenerater.generateRoute,
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IgnorePointer(
            ignoring: true,
            child: InkWell(
              onTap: () {
                Navigator.of(context).pushNamed(RouteGenerater.ROUTE_FIRST,
                    arguments: 'Main Page');
              },
              child: Container(
                alignment: Alignment.center,
                height: mediaQuery.size.height * 0.15,
                width: mediaQuery.size.width * 0.8,
                color: Colors.red,
                child: Text(
                  'Ignore Pointer',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: MediaQuery.of(context).size.width * 0.06,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 50,
                width: 100,
                color: Colors.blue,
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  height: 50,
                  color: Colors.green,
                  child: Text('Expanded'),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                flex: 3,
                child: Container(
                  alignment: Alignment.center,
                  height: 50,
                  color: Colors.blue,
                  child: Text('Flex 3'),
                ),
              ),
              Flexible(
                flex: 1,
                child: Container(
                  alignment: Alignment.center,
                  height: 50,
                  color: Colors.green,
                  child: Text('Flex 1'),
                ),
              ),
              Flexible(
                flex: 2,
                child: Container(
                  alignment: Alignment.center,
                  height: 50,
                  color: Colors.yellow,
                  child: Text('Flex 2'),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Stack(
            children: [
              Container(
                height: 70,
                width: 150,
                color: Colors.pink,
              ),
              Positioned(
                top: 40,
                left: 20,
                child: Text(
                  'Positioned',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 25,
          ),
          Text('ClipRRect'),
          SizedBox(
            height: 25,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(300.0),
            child: Image.network(
                'https://s.dou.ua/CACHE/images/img/static/companies/instagram_avatar_logo2_XutENMb/7df5537c994312d5f2a2b2776be0c7b0.png'),
          ),
        ],
      ),
    );
  }
}
