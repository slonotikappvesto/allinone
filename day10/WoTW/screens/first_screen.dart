import 'package:app/services/main_layout.dart';
import 'package:flutter/material.dart';

class FirstScreen extends StatelessWidget {
  final String lastPage;

  const FirstScreen(this.lastPage);

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('1'),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Current page: First Page'),
            Text('Last page: $lastPage'),
            SizedBox(
              height: 50,
            ),
            InkWell(
              onTap: () {
                Navigator.of(context)
                    .pushNamed('/ohnow', arguments: 'First Page');
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.redAccent,
                height: 70,
                width: 150,
                child: Text(
                  'New Page',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
