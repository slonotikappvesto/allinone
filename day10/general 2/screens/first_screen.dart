import 'package:app/services/main_layout.dart';
import 'package:app/services/router_generator.dart';
import 'package:flutter/material.dart';

class FirstScreen extends StatefulWidget {
  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  int _counter = 0;
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('1'),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop(this);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 70,
                    width: 170,
                    color: Colors.grey,
                    child: Text(
                      'Back',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: MediaQuery.of(context).size.width * 0.06,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed(RouteGenerater.ROUTE_SECOND);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 70,
                    width: 170,
                    color: Colors.grey,
                    child: Text(
                      'Image',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: MediaQuery.of(context).size.width * 0.06,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 50,
            ),
            Text(
              _counter.toString(),
              style: TextStyle(
                fontSize: 24,
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      _counter--;
                    });
                  },
                  child: Container(
                    height: 70,
                    width: 100,
                    alignment: Alignment.center,
                    color: Colors.redAccent,
                    child: Text(
                      '-',
                      style: TextStyle(
                        fontSize: 36,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      _counter++;
                    });
                  },
                  child: Container(
                    height: 70,
                    width: 100,
                    alignment: Alignment.center,
                    color: Colors.greenAccent,
                    child: Text(
                      '+',
                      style: TextStyle(
                        fontSize: 36,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
