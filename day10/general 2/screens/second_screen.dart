import 'package:app/services/main_layout.dart';
import 'package:app/services/router_generator.dart';
import 'package:flutter/material.dart';

class SecondtScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('2'),
      ),
      child: Center(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop(this);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 70,
                    width: 170,
                    color: Colors.grey,
                    child: Text(
                      'Back',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: MediaQuery.of(context).size.width * 0.06,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed(RouteGenerater.ROUTE_FIRST);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 70,
                    width: 170,
                    color: Colors.grey,
                    child: Text(
                      'Counter',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: MediaQuery.of(context).size.width * 0.06,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: 630,
              child: ListView(
                children: [
                  Image.network(
                      'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg'),
                  Image.network(
                      'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg'),
                  Image.network(
                      'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg'),
                  Image.network(
                      'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg'),
                  Image.network(
                      'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg'),
                  Image.network(
                      'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
