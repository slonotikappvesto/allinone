import 'package:app/services/main_layout.dart';
import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String lastPage;

  const ErrorScreen(this.lastPage);

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('Error'),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Last page: $lastPage'),
            SizedBox(
              height: 20,
            ),
            Text('Oh nnooooo'),
          ],
        ),
      ),
    );
  }
}
