import 'package:anim/screens/animation_screen.dart';
import 'package:anim/services/main_layout.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(HomePage());
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'anim',
      home: MainLayout(
        appBar: AppBar(
          title: Text('WoTW'),
        ),
        child: AnimationScreen(),
      ),
    );
  }
}
