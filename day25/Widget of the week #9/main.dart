import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wotw9',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _opacity = 1.0;
  var _color = Colors.red;
  var _showBlok = false;
  var _padValue = 10.0;
  int acceptedData = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  _opacity = 0.1;
                  _color = Colors.blue;
                  _showBlok = true;
                  _padValue = 100.0;
                });
              },
              child: AnimatedOpacity(
                opacity: _opacity,
                duration: const Duration(seconds: 2),
                child: Container(
                  alignment: Alignment.center,
                  width: 160,
                  height: 75,
                  color: Colors.greenAccent,
                  child: Text('AnimatedOpacity'),
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            AnimatedContainer(
              alignment: Alignment.center,
              width: 160,
              height: 75,
              color: _color,
              duration: Duration(seconds: 2),
              child: Text('AnimatedContainer'),
            ),
            SizedBox(
              width: double.infinity,
              height: 350,
              child: Stack(
                children: [
                  Positioned(
                    bottom: 10,
                    right: 10,
                    child: Container(
                      alignment: Alignment.center,
                      width: 250,
                      height: 250,
                      color: Colors.purple,
                      child: Text('AnimatedPositioned'),
                    ),
                  ),
                  AnimatedPositioned(
                    bottom: _showBlok ? 70 : 10,
                    right: 10,
                    top: _showBlok ? 50 : 150,
                    child: Container(
                      alignment: Alignment.center,
                      width: 80,
                      height: 50,
                      color: Colors.green,
                    ),
                    duration: Duration(seconds: 2),
                  ),
                ],
              ),
            ),
            AnimatedPadding(
              padding: EdgeInsets.only(left: _padValue),
              duration: Duration(seconds: 2),
              child: Container(
                color: Colors.blue,
                width: 120,
                height: 75,
                alignment: Alignment.center,
                child: Text('AnimatedPadding'),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Draggable<int>(
                  // Data is the value this Draggable stores.
                  data: 10,
                  child: Container(
                    height: 100.0,
                    width: 100.0,
                    color: Colors.lightGreenAccent,
                    child: const Center(
                      child: Text('Draggable'),
                    ),
                  ),
                  feedback: Container(
                    color: Colors.deepOrange,
                    height: 100,
                    width: 100,
                    child: const Icon(Icons.directions_run),
                  ),
                  childWhenDragging: Container(
                    height: 100.0,
                    width: 100.0,
                    color: Colors.pinkAccent,
                    child: const Center(
                      child: Text('Child When Dragging'),
                    ),
                  ),
                ),
                DragTarget<int>(
                  builder: (
                    BuildContext context,
                    List<dynamic> accepted,
                    List<dynamic> rejected,
                  ) {
                    return Container(
                      height: 100.0,
                      width: 100.0,
                      color: Colors.cyan,
                      child: Center(
                        child: Text('Value is updated to: $acceptedData'),
                      ),
                    );
                  },
                  onAccept: (int data) {
                    setState(() {
                      acceptedData += data;
                    });
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
