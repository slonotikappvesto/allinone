import 'package:canvas_pr/pie_data.dart';
import 'package:flutter/material.dart';

import 'my_custom_circle.dart';

void main() => runApp(DemoPageState());

class DemoPageState extends StatefulWidget {
  @override
  _DemoPageStateState createState() => _DemoPageStateState();
}

class _DemoPageStateState extends State<DemoPageState> {
  List<PieData> mData;

  PieData pieData;

  @override
  void initState() {
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: MyCustomCircle(mData, pieData),
        ),
      ),
    );
  }

  void initData() {
    mData = new List();

    double per = 0.14285;

    PieData p1 = new PieData();
    p1.percentage = per;
    p1.color = Color(0xff27b4f2);

    mData.add(p1);

    PieData p2 = new PieData();

    p2.percentage = per;
    p2.color = Color(0xfffc5c7c);
    mData.add(p2);

    PieData p3 = new PieData();

    p3.percentage = per;
    p3.color = Color(0xff00569c);
    mData.add(p3);

    PieData p4 = new PieData();

    p4.percentage = per;
    p4.color = Color(0xff6cdc55);
    mData.add(p4);

    PieData p5 = new PieData();

    p5.percentage = per;
    p5.color = Color(0xfff7d5a5);
    mData.add(p5);

    PieData p6 = new PieData();

    p6.percentage = per;
    p6.color = Color(0xff27b4f2);
    mData.add(p6);

    PieData p7 = new PieData();

    p7.percentage = per;
    p7.color = Color(0xfffc5c7c);
    mData.add(p7);
  }
}
