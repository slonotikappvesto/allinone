import 'dart:math';

import 'package:canvas_pr/pie_data.dart';
import 'package:flutter/material.dart';

class MyCustomCircle extends StatelessWidget {
  List<PieData> datas;

  PieData data;
  var dataSize;

  MyCustomCircle(this.datas, this.data);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(painter: MyView(datas, data));
  }
}

class MyView extends CustomPainter {
  Paint _mPaint;
  num mRadius, mInnerRadius, mBigRadius;
  num mStartAngle = 0;
  Rect mOval, mBigOval;

  List<PieData> mData;
  PieData pieData;

  MyView(this.mData, this.pieData);

  @override
  void paint(Canvas canvas, Size size) {
    _mPaint = new Paint();
    mRadius = 100.0;
    mBigRadius = 120.0;
    mInnerRadius = mRadius * 0.55;
    mOval = Rect.fromLTRB(-mRadius, -mRadius, mRadius, mRadius);
    mBigOval = Rect.fromLTRB(-mBigRadius, -mBigRadius, mBigRadius, mBigRadius);

    if (mData.length == null || mData.length <= 0) {
      return;
    }

    canvas.save();
    num startAngle = 0.0;
    for (int i = 0; i < mData.length; i++) {
      double sweepAngle = 2 * pi * mData[i].percentage;
      _mPaint..color = mData[i].color;

      int currentSelect = 1;
      if (currentSelect >= 0 && i == currentSelect) {
        canvas.drawArc(mBigOval, startAngle, sweepAngle, true, _mPaint);
      } else {
        canvas.drawArc(mOval, startAngle, sweepAngle, true, _mPaint);
      }
      startAngle += sweepAngle;
    }

    _mPaint..color = Colors.white;
    canvas.drawCircle(Offset.zero, mInnerRadius, _mPaint);

    canvas.restore();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
