import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:udemy17/providers/list.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => ListProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Udemy 17',
        home: HomePage(),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final listItems = Provider.of<ListProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('17'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () => listItems.generateList(),
                  child: Container(
                    alignment: Alignment.center,
                    width: 70,
                    height: 65,
                    color: Colors.greenAccent,
                    child: Text('Generate'),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                InkWell(
                  onTap: () => listItems.sortAD(),
                  child: Container(
                    alignment: Alignment.center,
                    width: 70,
                    height: 65,
                    color: Colors.greenAccent,
                    child: Text('Sort'),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                InkWell(
                  onTap: () => listItems.deleteRandom(),
                  child: Container(
                    alignment: Alignment.center,
                    width: 70,
                    height: 65,
                    color: Colors.greenAccent,
                    child: Text('Delete'),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
              ],
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              width: 300,
              height: 600,
              child: ListView.builder(
                itemCount: listItems.itemsList.length,
                itemBuilder: (context, index) {
                  return Container(
                    alignment: Alignment.center,
                    width: 200,
                    height: 50,
                    color: Colors.deepOrangeAccent,
                    child: Text(listItems.itemsList[index]),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
