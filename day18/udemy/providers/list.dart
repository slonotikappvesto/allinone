import 'dart:math';

import 'package:flutter/cupertino.dart';

class ListProvider with ChangeNotifier {
  var rng = new Random();

  var items = ['Click "Generate"'];

  void generateList() {
    items = List<String>.generate(
        rng.nextInt(50), (index) => '$index. It\'s so faster');
    notifyListeners();
  }

  void sortAD() {
    items = items.reversed.toList();
    notifyListeners();
  }

  void deleteRandom() {
    items.removeAt(rng.nextInt(items.length));
    notifyListeners();
  }

  List<String> get itemsList {
    return items;
  }
}
