import 'dart:ui';

import 'package:flutter/material.dart';

void main() => runApp(SimpleApp());

class SimpleApp extends StatefulWidget {
  SimpleApp({Key key}) : super(key: key);

  @override
  _SimpleAppState createState() => _SimpleAppState();
}

class _SimpleAppState extends State<SimpleApp> {
  List<bool> isSelected = [false, false, false];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
        body: Column(
          children: [
            SizedBox(
              height: 150,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Text('0' * 10000),
                  Center(
                    child: ClipRect(
                      child: BackdropFilter(
                        filter: ImageFilter.blur(
                          sigmaX: 5.0,
                          sigmaY: 5.0,
                        ),
                        child: Container(
                          alignment: Alignment.center,
                          width: 200.0,
                          height: 120.0,
                          child: const Text('BackdropFilter'),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            ShaderMask(
              shaderCallback: (Rect bounds) => RadialGradient(
                center: Alignment.topLeft,
                radius: 1.0,
                colors: <Color>[Colors.yellow, Colors.green],
                tileMode: TileMode.mirror,
              ).createShader(bounds),
              child: const Text(
                'ShaderMask ',
                style: TextStyle(fontSize: 32, color: Colors.white),
              ),
            ),
            Text('ToggleButtons:'),
            ToggleButtons(
              children: <Widget>[
                Icon(Icons.ac_unit),
                Icon(Icons.call),
                Icon(Icons.cake),
              ],
              onPressed: (int index) {
                setState(() {
                  isSelected[index] = !isSelected[index];
                });
              },
              isSelected: isSelected,
            ),
            SizedBox(
              height: 200,
              child: DraggableScrollableSheet(
                builder:
                    (BuildContext context, ScrollController scrollController) {
                  return Container(
                    color: Colors.blue[100],
                    child: ListView.builder(
                      controller: scrollController,
                      itemCount: 25,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                            title: Text('DraggableScrollableSheet  $index'));
                      },
                    ),
                  );
                },
              ),
            ),
            Text('ListWheelScrollView:'),
            SizedBox(
              height: 200,
              child: ListWheelScrollView(
                children: [
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.green,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.red,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.green,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.redAccent,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.black,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.orange,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.green,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.purpleAccent,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.orangeAccent,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.green,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.blueAccent,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.green,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.grey,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    width: 300,
                    height: 50,
                    color: Colors.purple,
                  ),
                ],
                itemExtent: 42,
                offAxisFraction: -0.5,
              ),
            )
          ],
        ),
      ),
    );
  }
}
