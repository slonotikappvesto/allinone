import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gen 15',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Wow'),
        ),
        body: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var items = List<String>.generate(100, (i) => "Item $i");

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CustomPaint(
        painter: OpenPainter(MediaQuery.of(context).size.height * 0.25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  items = items.reversed.toList();
                });
              },
              child: Container(
                alignment: Alignment.center,
                width: 150,
                height: 70,
                color: Colors.greenAccent,
                child: Text('Click'),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              width: 300,
              height: 400,
              child: ListView.builder(
                itemCount: items.length,
                itemBuilder: (context, index) => Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.all(4),
                    width: 270,
                    height: 50,
                    color: Colors.redAccent,
                    child: Text('${items[index]}')),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class OpenPainter extends CustomPainter {
  final double sized;

  OpenPainter(this.sized);

  @override
  void paint(Canvas canvas, Size size) {
    var paint1 = Paint()
      ..color = Colors.yellow
      ..style = PaintingStyle.fill;
    //a circle
    canvas.drawCircle(Offset(sized, sized), 100, paint1);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
