import 'package:flutter/material.dart';
import 'package:prac13/providers/counter.dart';
import 'package:prac13/screens/btn_screen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => Counter(),
      child: MaterialApp(
        title: 'Counter',
        home: ButtonScreen(),
      ),
    );
  }
}
