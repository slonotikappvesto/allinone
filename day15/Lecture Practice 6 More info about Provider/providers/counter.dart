import 'package:flutter/cupertino.dart';
import 'dart:math';

class Counter with ChangeNotifier {
  var _counter = 0;
  var _counterNew = 0;

  int get getCounter {
    return _counter;
  }

  int get getCounterNew {
    return _counterNew;
  }

  void incCounter() {
    Random random = new Random();
    int randomNumber = random.nextInt(2);
    if (randomNumber == 0) {
      _counter++;
    } else {
      _counterNew++;
    }
    notifyListeners();
  }

  void decCounter() {
    Random random = new Random();
    int randomNumber = random.nextInt(2);
    if (randomNumber == 0) {
      _counter++;
    } else {
      _counterNew++;
    }
    notifyListeners();
  }
}
