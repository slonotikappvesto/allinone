import 'package:flutter/material.dart';
import 'package:prac13/providers/counter.dart';
import 'package:prac13/widgets/myButton.dart';
import 'package:provider/provider.dart';

class ButtonScreen extends StatelessWidget {
  static const routeName = '/button';
  @override
  Widget build(BuildContext context) {
    final counter = Provider.of<Counter>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Text('Button'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Selector<Counter, int>(
              builder: (ctx, data, _) {
                return Text('$data');
              },
              selector: (context, countPro) => countPro.getCounter,
            ),
            SizedBox(
              height: 40,
            ),
            Text(counter.getCounterNew.toString()),
            SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyButton(() {
                  counter.decCounter();
                }, '-'),
                SizedBox(
                  width: 20,
                ),
                MyButton(() {
                  counter.incCounter();
                }, '+'),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
