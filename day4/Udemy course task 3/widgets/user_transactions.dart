import 'package:expapp/models/transaction.dart';
import 'package:expapp/widgets/transaction_list.dart';
import 'package:flutter/material.dart';

import 'new_transaction.dart';

class UserTransactions extends StatefulWidget {
  const UserTransactions({Key key}) : super(key: key);

  @override
  _UserTransactionsState createState() => _UserTransactionsState();
}

class _UserTransactionsState extends State<UserTransactions> {
  final List<Transaction> _userTransactions = [
    Transaction(
      id: 't1',
      title: 'New Car',
      amount: 699.99,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't2',
      title: 'Fruits',
      amount: 9.42,
      date: DateTime.now(),
    ),
    Transaction(
      id: 't3',
      title: 'Surprise',
      amount: 69.99,
      date: DateTime.now(),
    ),

    Transaction(
      id: 't5',
      title: 'Surprise 2',
      amount: 69.99,
      date: DateTime.now(),
    ),

    Transaction(
      id: 't4',
      title: 'Surprise 3',
      amount: 69.99,
      date: DateTime.now(),
    ),

    Transaction(
      id: 't6',
      title: 'Surprise 4',
      amount: 69.99,
      date: DateTime.now(),
    ),
  ];

  void _addNewTransaction(String title, double amount) {
    final newTransactions = Transaction(
      id: DateTime.now().toString(),
      title: title,
      amount: amount,
      date: DateTime.now(),
    );

    setState(() {
      _userTransactions.add(newTransactions);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NewTransaction(_addNewTransaction),
        TransactionList(_userTransactions),
      ],
    );
  }
}
