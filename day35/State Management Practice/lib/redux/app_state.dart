import 'package:flutter/material.dart';

class AppState {
  final int counter;
  final String text;
  final Color colorAppBar;

  AppState({
    @required this.counter,
    @required this.text,
    @required this.colorAppBar,
  });

  AppState copyWith({int counter, String text}) {
    return AppState(
      counter: counter ?? this.counter,
      text: text ?? this.text,
      colorAppBar: colorAppBar ?? this.colorAppBar,
    );
  }
}
