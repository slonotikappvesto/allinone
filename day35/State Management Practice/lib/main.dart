import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:reduxsimple/dto.dart';
import 'package:reduxsimple/redux/app_state.dart';
import 'package:http/http.dart' as http;
import 'redux/actions.dart';
import 'redux/reducers.dart';

Stream<dynamic> wetherEpic(
    Stream<dynamic> actions, EpicStore<AppState> store) async* {
  yield SetTextAction(text: 'Odessa, clouds: 76');
}

void main() {
  final Store<AppState> store = Store(reducer,
      initialState: AppState(
          counter: 0, text: "Waiting...", colorAppBar: Colors.grey[800]),
      middleware: [EpicMiddleware(wetherEpic)]);
  runApp(StoreProvider(
    store: store,
    child: MaterialApp(
      home: Counter(),
    ),
  ));
}

class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  bool _isSwitch = false;

  @override
  Widget build(BuildContext context) {
    final Store<AppState> store = StoreProvider.of<AppState>(context);
    String inputText = "";
    return Scaffold(
      appBar: PreferredSize(
        child: StoreConnector<AppState, AppState>(
          converter: (store) => store.state,
          builder: (context, vm) => AppBar(
            backgroundColor: vm.colorAppBar,
          ),
        ),
        preferredSize: const Size.fromHeight(45),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => store.dispatch(AddAction()),
        child: Icon(
          Icons.fire_hydrant,
        ),
      ),
      drawer: Drawer(
        child: Padding(
          padding: const EdgeInsets.only(top: 48.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                onPressed: () {
                  store.dispatch(
                    SetAppBarColor(color: Colors.grey[850]),
                  );
                },
                child: Text("Grey"),
              ),
              TextButton(
                onPressed: () {
                  store.dispatch(
                    SetAppBarColor(color: Colors.orange),
                  );
                },
                child: Text("Orange"),
              ),
              TextButton(
                onPressed: () {
                  store.dispatch(
                    SetAppBarColor(color: Colors.blue),
                  );
                },
                child: Text("Blue"),
              ),
              TextButton(
                onPressed: () {
                  store.dispatch(
                    SetAppBarColor(color: Colors.white),
                  );
                },
                child: Text("White"),
              ),
            ],
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [],
        ),
      ),
    );
  }
}
