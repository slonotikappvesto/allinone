import 'package:flutter/cupertino.dart';

class WeatherDTO {
  final int obl;

  WeatherDTO({@required this.obl});

  factory WeatherDTO.fromJson(Map<String, dynamic> json) {
    return WeatherDTO(obl: json['clouds']['all']);
  }
}
