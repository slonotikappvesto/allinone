import 'package:redux/redux.dart';
import 'package:reduxsecpr/dto/data_dto.dart';
import 'package:reduxsecpr/store/app/app_state.dart';
import 'package:reduxsecpr/store/pages/data_state/data_selector.dart';

class DataPageViewModel {
  final List<DataDto> data;
  final void Function() getData;

  DataPageViewModel({this.data, this.getData});

  static DataPageViewModel fromStore(Store<AppState> store) {
    return DataPageViewModel(
      data: DataSelector.getCurrentData(store),
      getData: DataSelector.getDataFunction(store),
    );
  }
}
