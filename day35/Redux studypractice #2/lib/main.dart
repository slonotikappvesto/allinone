import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:reduxsecpr/data_view_model.dart';
import 'package:reduxsecpr/store/app/app_state.dart';

void main() {
  Store store = Store<AppState>(AppState.getAppReducer,
      initialState: AppState.initial(),
      middleware: [
        EpicMiddleware(AppState.getAppEpic),
      ]);
  runApp(MyApp(store: store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  const MyApp({@required this.store, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: DataPage(),
      ),
    );
  }
}

class DataPage extends StatelessWidget {
  const DataPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, DataPageViewModel>(
      converter: DataPageViewModel.fromStore,
      onInitialBuild: (DataPageViewModel dataViewModel) =>
          dataViewModel.getData(),
      builder: (BuildContext context, DataPageViewModel dataViewModel) {
        return MaterialApp(
          home: Scaffold(
            appBar: AppBar(
              title: Text('Data API'),
              centerTitle: true,
            ),
            body: Container(
              child: ListView.builder(
                itemCount: dataViewModel.data.length,
                itemBuilder: (BuildContext ctx, int index) => Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(60.0),
                    child: Container(
                      alignment: Alignment.center,
                      height: 70.0,
                      width: 250,
                      color: Colors.greenAccent,
                      margin: const EdgeInsets.all(10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            dataViewModel.data[index].name,
                          ),
                          Text(
                              'City: ${dataViewModel.data[index].city} \nState: ${dataViewModel.data[index].state}')
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
