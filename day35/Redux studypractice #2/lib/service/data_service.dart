import 'package:reduxsecpr/network_service.dart';
import 'package:reduxsecpr/service/base_network_service.dart';

class DataService {
  DataService._privateConstructor();

  static final DataService instance = DataService._privateConstructor();
  static String _url = 'https://api.openbrewerydb.org/breweries';

  Future<BaseNetworkService> getData() async {
    var responseData =
        await NetworkService.instance.request(HttpType.get, _url, null, null);
    print(responseData.response);
    return responseData;
  }
}
