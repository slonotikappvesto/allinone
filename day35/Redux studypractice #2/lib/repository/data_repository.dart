import 'package:reduxsecpr/dto/data_dto.dart';
import 'package:reduxsecpr/service/data_service.dart';

class DataRepository {
  DataRepository._privateConstructor();

  static final DataRepository instance = DataRepository._privateConstructor();

  Future<List<DataDto>> getData() async {
    var responseData = await DataService.instance.getData();
    List<DataDto> data = [];
    for (var item in responseData.response) {
      data.add(DataDto.fromJson(item));
    }
    return data;
  }
}
