import 'package:flutter/foundation.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:reduxsecpr/store/pages/data_state/data_epic.dart';
import 'package:reduxsecpr/store/pages/data_state/data_state.dart';

class AppState {
  final DataPageState dataPageState;
  AppState({
    @required this.dataPageState,
  });

  factory AppState.initial() {
    return AppState(
      dataPageState: DataPageState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      dataPageState: state.dataPageState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([
    DataEpics.indexEpic,
  ]);
}
