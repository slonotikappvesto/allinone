import 'dart:collection';

import 'package:reduxsecpr/dto/data_dto.dart';
import 'package:reduxsecpr/store/app/reducer.dart';
import 'package:reduxsecpr/store/pages/data_state/data_action.dart';

class DataPageState {
  final List<DataDto> dataList;

  DataPageState({this.dataList});

  factory DataPageState.initial() {
    return DataPageState(
      dataList: [],
    );
  }

  DataPageState copyWith({
    List<DataDto> dataList,
  }) {
    return DataPageState(
      dataList: dataList ?? this.dataList,
    );
  }

  DataPageState reducer(dynamic action) {
    return Reducer<DataPageState>(
      actions: HashMap.from({
        SetInitDataAction: (dynamic action) =>
            _saveItems((action as SetInitDataAction).data),
      }),
    ).updateState(action, this);
  }

  DataPageState _saveItems(List<DataDto> dataList) {
    return copyWith(dataList: dataList);
  }
}
