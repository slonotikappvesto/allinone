import 'package:reduxsecpr/dto/data_dto.dart';

class GetDataAction {}

class SetInitDataAction {
  final List<DataDto> data;
  SetInitDataAction({this.data});
}

class SelectDataAction {
  String id;
  SelectDataAction({this.id});
}
