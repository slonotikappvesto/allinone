import 'package:redux_epics/redux_epics.dart';
import 'package:reduxsecpr/dto/data_dto.dart';
import 'package:reduxsecpr/repository/data_repository.dart';
import 'package:reduxsecpr/store/app/app_state.dart';
import 'package:reduxsecpr/store/shared/empty_action.dart';
import 'package:rxdart/rxdart.dart';
import 'package:reduxsecpr/store/pages/data_state/data_action.dart';

class DataEpics {
  static final indexEpic = combineEpics<AppState>([
    getDataEpic,
  ]);

  static Stream<dynamic> getDataEpic(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetDataAction>().switchMap(
      (action) {
        return Stream.fromFuture(
          DataRepository.instance.getData().then(
            (List<DataDto> data) {
              if (data == null) {
                return EmptyAction();
              }
              return SetInitDataAction(data: data);
            },
          ),
        );
      },
    );
  }
}
