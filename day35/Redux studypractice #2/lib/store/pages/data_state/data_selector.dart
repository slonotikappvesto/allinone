import 'package:redux/redux.dart';
import 'package:reduxsecpr/dto/data_dto.dart';
import 'package:reduxsecpr/store/app/app_state.dart';
import 'package:reduxsecpr/store/pages/data_state/data_action.dart';

class DataSelector {
  static void Function() getDataFunction(Store<AppState> store) {
    return () => store.dispatch(GetDataAction());
  }

  static List<DataDto> getCurrentData(Store<AppState> store) {
    return store.state.dataPageState.dataList;
  }
}
