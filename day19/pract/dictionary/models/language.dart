import 'package:flutter/cupertino.dart';
import 'package:udemy19/dictionary/dictionary_classes/genaral_language.dart';
import 'package:udemy19/dictionary/dictionary_classes/home_page_language.dart';

class Language {
  final GeneralLanguage generalLanguage;
  final HomePageLanguage homePageLanguage;

  const Language({
    @required this.generalLanguage,
    @required this.homePageLanguage,
  });
}
