import 'package:udemy19/dictionary/dictionary_classes/genaral_language.dart';
import 'package:udemy19/dictionary/dictionary_classes/home_page_language.dart';
import 'package:udemy19/dictionary/models/language.dart';

const Language ru = Language(
  generalLanguage: GeneralLanguage(
    appTitle: 'Test App',
  ),
  homePageLanguage: HomePageLanguage(
    fresh: 'Русский!',
    money: '\$',
  ),
);
