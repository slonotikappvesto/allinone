import 'package:flutter/cupertino.dart';

class HomePageLanguage {
  final String fresh;
  final String money;

  const HomePageLanguage({
    @required this.fresh,
    @required this.money,
  });

  factory HomePageLanguage.fromJson(Map<String, dynamic> json) {
    return HomePageLanguage(
      fresh: json['fresh'],
      money: json['money'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'fresh': fresh,
      'money': money,
    };
  }
}
