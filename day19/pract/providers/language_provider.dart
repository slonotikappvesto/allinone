import 'package:flutter/cupertino.dart';
import 'package:udemy19/dictionary/flutter_dictionary.dart';

class LanguageProvider extends ChangeNotifier {
  void setNewLane(Locale locale) {
    FlutterDictionary.instance.setNewLanguage(locale.toString());
    notifyListeners();
  }
}
