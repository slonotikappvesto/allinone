import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:udemy19/dictionary/data/ru.dart';
import 'package:udemy19/dictionary/flutter_delegate.dart';
import 'package:udemy19/providers/language_provider.dart';

import 'dictionary/data/en.dart';
import 'dictionary/dictionary_classes/home_page_language.dart';
import 'dictionary/flutter_dictionary.dart';
import 'dictionary/models/supported_locales.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      builder: (BuildContext ctx) {
        return LanguageProvider();
      },
      child: Application(),
    ),
  );
}

class Application extends StatefulWidget {
  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {
  @override
  Widget build(BuildContext context) {
    HomePageLanguage language =
        FlutterDictionary.instance.language?.homePageLanguage ??
            en.homePageLanguage;
    return Consumer<LanguageProvider>(
      builder: (BuildContext context, LanguageProvider provider, Widget _) {
        return MaterialApp(
          theme: ThemeData.dark(),
          debugShowCheckedModeBanner: false,
          locale: Locale(FlutterDictionaryDelegate.getCurrentLocale),
          supportedLocales: SupportedLocales.instance.getSupportedLocales,
          localizationsDelegates:
              FlutterDictionaryDelegate.getLocalizationDelegates,
          home: Scaffold(
            appBar: AppBar(
              title: Text('Data man'),
            ),
            body: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 25,
                  ),
                  Text(language.fresh),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      for (Locale loc
                          in SupportedLocales.instance.getSupportedLocales)
                        InkWell(
                          onTap: () {
                            provider.setNewLane(loc);
                            setState(() {});
                          },
                          child: AnimatedContainer(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: FlutterDictionaryDelegate.getCurrentLocale
                                          .toString() ==
                                      loc.toString()
                                  ? Colors.green
                                  : Colors.grey,
                            ),
                            padding: EdgeInsets.all(15),
                            duration: Duration(milliseconds: 400),
                            child: Text(
                              loc.toString().toUpperCase(),
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
