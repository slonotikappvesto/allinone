import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

void main() {
  runApp(MyHomePage());
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future getFromGallery() async {
    PickedFile pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'amazing',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Image Picker'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    color: Colors.green,
                    child: _image == null
                        ? Text('No image selected.')
                        : Image.file(_image)),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: getFromGallery,
                    child: Container(
                      alignment: Alignment.center,
                      height: 60,
                      width: 180,
                      color: Colors.greenAccent,
                      child: Text('Pick from gallery'),
                    ),
                  ),
                  InkWell(
                    onTap: getImage,
                    child: Container(
                      alignment: Alignment.center,
                      height: 60,
                      width: 180,
                      color: Colors.greenAccent,
                      child: Text('Pick from camera'),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
