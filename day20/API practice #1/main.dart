import 'dart:convert';

import 'package:day20dao/models/user.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DTO Day20',
      home: Scaffold(
        appBar: AppBar(
          title: Text('DTO day 20'),
        ),
        body: DataFromAPI(),
      ),
    );
  }
}

class DataFromAPI extends StatefulWidget {
  @override
  _DataFromAPIState createState() => _DataFromAPIState();
}

class _DataFromAPIState extends State<DataFromAPI> {
  User user;

  getUserData() async {
    var response = await http.get('https://randomuser.me/api/');
    var jsonData = jsonDecode(response.body);
    setState(() {
      user = User.fromJson(jsonData);
    });
    print(user.name);
  }

  @override
  void initState() {
    super.initState();
    getUserData();
  }

  @override
  Widget build(BuildContext context) {
    return user == null
        ? Center(child: CircularProgressIndicator())
        : Container(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('${user.name}, ${user.age} y.o.'),
                SizedBox(
                  height: 16,
                ),
                Text(user.email),
                SizedBox(
                  height: 16,
                ),
                Image.network(user.image),
                SizedBox(
                  height: 16,
                ),
                InkWell(
                  onTap: getUserData,
                  child: Icon(
                    Icons.refresh,
                    size: 32,
                  ),
                ),
              ],
            ),
          );
  }
}
