class User {
  final String name, email, image;
  final int age;

  User({this.name, this.email, this.image, this.age});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      name:
          '${json['results'][0]['name']['title']} ${json['results'][0]['name']['first']} ${json['results'][0]['name']['last']}',
      email: json['results'][0]['email'],
      image: json['results'][0]['picture']['large'],
      age: json['results'][0]['registered']['age'],
    );
  }
}
