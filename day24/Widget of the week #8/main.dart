import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'WoTW 8';

    return MaterialApp(
      title: appTitle,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: DataPage(),
    );
  }
}

class DataPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          InkWell(
            onTap: () => showDialog(
              context: context,
              builder: (_) => AlertDialog(
                title: Text('Wow?'),
                content: Text('REALY?'),
                actions: [
                  Text('No'),
                  Text('Yes'),
                ],
              ),
            ),
            child: Container(
              alignment: Alignment.center,
              width: 120,
              height: 50,
              color: Colors.green[100],
              child: Text('Alert Dialog'),
            ),
          ),
          InkWell(
            onTap: () => showCupertinoModalPopup(
                context: context,
                builder: (context) => CupertinoActionSheet(
                      title: Text('What?'),
                      message: Text('A short list llooolllll'),
                      actions: [
                        CupertinoActionSheetAction(
                          child: Text('Thing 1'),
                          onPressed: () {},
                        ),
                        CupertinoActionSheetAction(
                          child: Text('Thing 2'),
                          onPressed: () {},
                        ),
                        CupertinoActionSheetAction(
                          child: Text('Thing 3'),
                          onPressed: () {},
                        ),
                      ],
                    )),
            child: Container(
              alignment: Alignment.center,
              width: 160,
              height: 50,
              color: Colors.red[100],
              child: Text('CupertinoActionSheet'),
            ),
          ),
          InkWell(
            onTap: () {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('Hello, best программист'),
                ),
              );
            },
            child: Container(
              alignment: Alignment.center,
              width: 120,
              height: 50,
              color: Colors.orange[100],
              child: Text('Snackbar'),
            ),
          ),
          CircularProgressIndicator(),
          CupertinoActivityIndicator(),
        ],
      ),
    );
  }
}
