import 'package:flutter/foundation.dart';

class DataDto {
  final int id;
  final String name;
  final String city;
  final String state;

  DataDto({
    @required this.id,
    @required this.name,
    @required this.city,
    @required this.state,
  });

  factory DataDto.fromJson(Map<String, dynamic> json) {
    return DataDto(
      id: json["id"],
      name: json["name"],
      city: json["city"],
      state: json["state"],
    );
  }
}
