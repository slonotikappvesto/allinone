import 'package:reduxsecpr/network_service.dart';
import 'package:reduxsecpr/service/base_network_service.dart';

class UnsplashImageService {
  UnsplashImageService._privateConstructor();

  static final UnsplashImageService instance =
      UnsplashImageService._privateConstructor();
  static String _url =
      'https://api.unsplash.com/photos/?client_id=mydqlxMVVuFMchsrB5WTCY99QkZGE9ZUJHq_sJdqy4s';

  Future<BaseNetworkService> getData() async {
    var responseData =
        await NetworkService.instance.request(HttpType.get, _url, null, null);
    print(responseData.response);
    return responseData;
  }
}
