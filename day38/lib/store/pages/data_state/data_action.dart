import 'package:reduxsecpr/dto/data_dto.dart';

class GetData {}

class SetInitDataAction {
  final List<DataDto> images;
  SetInitDataAction({this.images});
}

class SaveDataAction {
  List<DataDto> images;
  SaveDataAction({this.images});
}

class SelectDataAction {
  String id;
  SelectDataAction({this.id});
}
