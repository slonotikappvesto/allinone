import 'package:reduxsecpr/dto/data_dto.dart';
import 'package:reduxsecpr/service/unspash_image_service.dart';

class UnsplashImageRepository {
  UnsplashImageRepository._privateConstructor();

  static final UnsplashImageRepository instance =
      UnsplashImageRepository._privateConstructor();

  Future<List<DataDto>> getImages() async {
    var responseData = await UnsplashImageService.instance.getData();
    List<DataDto> data = [];
    for (var item in responseData.response) {
      data.add(DataDto.fromJson(item));
    }
    return data;
  }
}
