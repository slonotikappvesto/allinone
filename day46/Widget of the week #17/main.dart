import 'dart:ui';

import 'package:flutter/material.dart';

void main() => runApp(SimpleApp());

class SimpleApp extends StatefulWidget {
  SimpleApp({Key key}) : super(key: key);

  @override
  _SimpleAppState createState() => _SimpleAppState();
}

class _SimpleAppState extends State<SimpleApp> with TickerProviderStateMixin {
  AnimationController _controller;

  List<int> _list = [];
  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();

  void _addItem() {
    final int _index = _list.length;
    _list.insert(_index, _index);
    _listKey.currentState.insertItem(_index);
  }

  void _removeItem() {
    final int _index = _list.length - 1;
    _listKey.currentState
        .removeItem(_index, (context, animation) => Container());

    /// what I'm supposed to do here
    _list.removeAt(_index);
  }

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  List<bool> isSelected = [false, false, false];
  double opacity = 1.0;

  Widget red = Container(
    height: 60,
    width: 190,
    color: Colors.red,
  );

  bool _first = false;

  final List<int> _items = List<int>.generate(10, (int index) => index);

  @override
  Widget build(BuildContext context) {
    final ColorScheme colorScheme = Theme.of(context).colorScheme;
    final Color oddItemColor = colorScheme.primary.withOpacity(0.05);
    final Color evenItemColor = colorScheme.primary.withOpacity(0.15);
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              color: Colors.greenAccent,
              height: 200,
              child: AnimatedList(
                key: _listKey,
                initialItemCount: 0,
                itemBuilder:
                    (BuildContext context, int index, Animation animation) {
                  return _buildItem(_list[index].toString(), animation);
                },
              ),
            ),
            AnimatedIcon(
              icon: AnimatedIcons.play_pause,
              size: 64,
              progress: _controller,
            ),
            Container(
              height: 200,
              color: Colors.blueGrey[200],
              child: ReorderableListView(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                children: <Widget>[
                  for (int index = 0; index < _items.length; index++)
                    ListTile(
                      key: Key('$index'),
                      tileColor:
                          _items[index].isOdd ? oddItemColor : evenItemColor,
                      title: Text('Item ${_items[index]}'),
                    ),
                ],
                onReorder: (int oldIndex, int newIndex) {
                  setState(() {
                    if (oldIndex < newIndex) {
                      newIndex -= 1;
                    }
                    final int item = _items.removeAt(oldIndex);
                    _items.insert(newIndex, item);
                  });
                },
              ),
            ),
            IndexedStack(
              children: [
                Container(
                  alignment: Alignment.center,
                  width: 100,
                  height: 100,
                  color: Colors.green,
                  child: Text('IndexedStack '),
                ),
                Container(
                  width: 150,
                  height: 50,
                  color: Colors.red,
                ),
              ],
            )
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              FloatingActionButton(
                backgroundColor: Colors.green,
                onPressed: () => _addItem(),
                tooltip: 'Increment',
                child: Icon(Icons.add),
              ),
              FloatingActionButton(
                backgroundColor: Colors.green,
                onPressed: () => _removeItem(),
                tooltip: 'Decrement',
                child: Icon(Icons.remove),
              ),
            ],
          ),
        ],
      ),
    ));
  }

  Widget _buildItem(String _item, Animation _animation) {
    return SizeTransition(
      sizeFactor: _animation,
      child: Card(
        color: Colors.blue[50],
        child: ListTile(
          title: Text(
            _item,
          ),
        ),
      ),
    );
  }
}
