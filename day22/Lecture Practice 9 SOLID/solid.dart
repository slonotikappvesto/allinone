import 'dart:math';

abstract class Shape {
  double calcArea() {
    return 0.0;
  }
}

class Circle extends Shape {
  final double radius;
  Circle(this.radius);

  @override
  calcArea() {
    return pi * (radius * radius);
  }
}

class Square extends Shape {
  final double lenght;
  Square(this.lenght);

  @override
  calcArea() {
    return lenght * lenght;
  }
}

class AreaCalculator {
  final List<dynamic> shapes;

  AreaCalculator(this.shapes);

  void output() {
    shapes.forEach((element) {
      print(element);
    });
  }
}

class WowArea extends AreaCalculator {
  WowArea(List<dynamic> shapes) : super(shapes);

  void sum() {
    shapes.forEach((element) {
      print('Area: ${element.calcArea()}');
    });
  }
}

abstract class SomeAbstractClass {
  someMethod() {
    print('Method from SomeAbstractClass');
  }
}

class SomeClass extends SomeAbstractClass {
  @override
  someMethod() {
    print('Method from SomeClass');
  }
}

class AnotherClass {
  final List<SomeAbstractClass> someAbstractClassValues;

  AnotherClass(this.someAbstractClassValues);
}
