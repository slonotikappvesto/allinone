import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Layout building practice',
      home: HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  int currentState = 0;

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: [
          Positioned(
            top: -width * 0.5,
            right: -width * 0.3,
            child: Container(
              height: height * 0.8,
              width: width * 0.8,
              decoration: BoxDecoration(
                color: Color(0xFFe95d55),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: -height * 0.02,
            left: width * 0.35,
            child: Container(
              height: height * 0.6,
              width: width * 0.6,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xFFe95d55),
                  width: 2.0,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: height * 0.1,
            left: width * 0.85,
            child: Container(
              height: height * 0.3,
              width: width * 0.3,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xFFf4eadd),
                  width: 2.0,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: height * 0.6,
            left: -width * 0.25,
            child: Container(
              height: height * 0.7,
              width: width * 0.7,
              decoration: BoxDecoration(
                color: Color(0xFFe95d55),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: height * 0.75,
            left: width * 0.1,
            child: Container(
              height: height * 0.5,
              width: width * 0.5,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xFFe95d55),
                  width: 2.0,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: height * 0.65,
            left: -width * 0.1,
            child: Container(
              height: height * 0.3,
              width: width * 0.3,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xFFf4eadd),
                  width: 2.0,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Container(
              height: height * 0.1,
              width: width * 0.9,
              margin: EdgeInsets.only(
                  top: height * 0.05,
                  right: height * 0.04,
                  left: height * 0.04),
              decoration: BoxDecoration(
                border: Border.symmetric(
                  horizontal: BorderSide(
                    width: 2,
                  ),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Custom APP',
                        style: TextStyle(fontSize: width * 0.04),
                      ),
                      IconButton(
                          icon: Icon(
                            Icons.menu,
                            size: width * 0.04,
                          ),
                          onPressed: () {})
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: height * 0.7,
            left: 0,
            right: 0,
            bottom: 12,
            child: Container(
              width: double.infinity,
              height: 150,
              child: CarouselSlider(
                options: CarouselOptions(
                  height: height * 0.2,
                  viewportFraction: 0.25,
                  onPageChanged: (index, reason) {
                    print('$index это индекс нигга');
                    setState(() {
                      currentState = index;
                    });
                  },
                ),
                items: [0, 1, 2, 3, 4].map((i) {
                  print('$currentState $i ${currentState == i}');
                  return Builder(
                    builder: (BuildContext context) {
                      return Column(
                        children: [
                          AnimatedOpacity(
                            opacity: currentState == i ? 1 : 0.6,
                            duration: Duration(milliseconds: 500),
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.2,
                              height: height * 0.14,
                              decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.5),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: Icon(
                                Icons.android_sharp,
                                color: currentState == i
                                    ? Colors.yellow
                                    : Colors.white60,
                                size: 42,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: height * 0.01,
                          ),
                          AnimatedOpacity(
                            opacity: currentState == i ? 1 : 0,
                            duration: Duration(milliseconds: 500),
                            child: Container(
                              width: width * 0.12,
                              height: height * 0.01,
                              decoration: BoxDecoration(
                                  color: Colors.yellow,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                          )
                        ],
                      );
                    },
                  );
                }).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
