import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
        body: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DateTime _chosenDateTime;

  void _showDatePicker(ctx) {
    // showCupertinoModalPopup is a built-in function of the cupertino library
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
              height: 500,
              color: Color.fromARGB(255, 255, 255, 255),
              child: Column(
                children: [
                  Container(
                    height: 400,
                    child: CupertinoDatePicker(
                        initialDateTime: DateTime.now(),
                        onDateTimeChanged: (val) {
                          setState(() {
                            _chosenDateTime = val;
                          });
                        }),
                  ),

                  // Close the modal
                  CupertinoButton(
                    child: Text('OK'),
                    onPressed: () => Navigator.of(ctx).pop(),
                  )
                ],
              ),
            ));
  }

  bool _switchValue = false;
  double _progress = 0.0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              _showDatePicker(context);
            },
            child: Container(
              alignment: Alignment.center,
              width: 120,
              height: 60,
              color: Colors.greenAccent,
              child: Text('Time Picker'),
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Text(_chosenDateTime != null
              ? _chosenDateTime.toString()
              : 'No date time picked!'),
          SizedBox(
            height: 25,
          ),
          InkWell(
            child: CupertinoSwitch(
              trackColor: Colors.red,
              activeColor: Colors.green,
              value: _switchValue,
              onChanged: (bool value) {
                setState(() {
                  _switchValue = value;
                });
              },
            ),
            onTap: () {
              setState(() {
                _switchValue = !_switchValue;
              });
            },
          ),
          SizedBox(
            height: 25,
          ),
          Text(_progress.toStringAsFixed(1)),
          CupertinoSlider(
            value: _progress,
            min: 0.0,
            max: 100.0,
            onChanged: (value) {
              setState(() {
                _progress = value;
              });
            },
          )
        ],
      ),
    );
  }
}
