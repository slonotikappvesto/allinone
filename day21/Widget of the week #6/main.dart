import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('WOTW #6'),
        ),
        body: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = PageController(initialPage: 1);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Hero & ColorFiltered'),
            SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BirdImage(),
                  ),
                );
              },
              child: Hero(
                tag: 'birds',
                child: ColorFiltered(
                  colorFilter:
                      ColorFilter.mode(Colors.yellow, BlendMode.multiply),
                  child: Image.network(
                    'https://images2.minutemediacdn.com/image/upload/c_crop,h_1188,w_2121,x_0,y_142/v1554733134/shape/mentalfloss/78996-istock-682216682.jpg?itok=5o35rflV',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Text('PageView:'),
            SizedBox(
              height: 40,
            ),
            Container(
              width: double.infinity,
              height: 200,
              child: PageView(
                controller: controller,
                scrollDirection: Axis.horizontal,
                children: [
                  ColorFiltered(
                    colorFilter:
                        ColorFilter.mode(Colors.white54, BlendMode.multiply),
                    child: Image.network(
                      'https://images2.minutemediacdn.com/image/upload/c_crop,h_1188,w_2121,x_0,y_142/v1554733134/shape/mentalfloss/78996-istock-682216682.jpg?itok=5o35rflV',
                      fit: BoxFit.fill,
                    ),
                  ),
                  ColorFiltered(
                    colorFilter:
                        ColorFilter.mode(Colors.red, BlendMode.multiply),
                    child: Image.network(
                      'https://images2.minutemediacdn.com/image/upload/c_crop,h_1188,w_2121,x_0,y_142/v1554733134/shape/mentalfloss/78996-istock-682216682.jpg?itok=5o35rflV',
                      fit: BoxFit.fill,
                    ),
                  ),
                  ColorFiltered(
                    colorFilter:
                        ColorFilter.mode(Colors.green, BlendMode.multiply),
                    child: Image.network(
                      'https://images2.minutemediacdn.com/image/upload/c_crop,h_1188,w_2121,x_0,y_142/v1554733134/shape/mentalfloss/78996-istock-682216682.jpg?itok=5o35rflV',
                      fit: BoxFit.fill,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: AspectRatio(
                  aspectRatio: 9 / 2,
                  child: Container(
                    alignment: Alignment.center,
                    color: Colors.red,
                    child: Text(
                      'AspectRation',
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BirdImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sweet birds'),
      ),
      body: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Text(
            'BIRDS',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 64),
          ),
          SizedBox(
            height: 14,
          ),
          Hero(
            tag: 'birds',
            child: Image.network(
              'https://images2.minutemediacdn.com/image/upload/c_crop,h_1188,w_2121,x_0,y_142/v1554733134/shape/mentalfloss/78996-istock-682216682.jpg?itok=5o35rflV',
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            height: 60,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28.0),
            child: Text(
              'When owls catch larger animals (raccoons and rabbits, for instance), they tear them up into more manageable, bite-size pieces. But, theyve also been known to simply swallow smaller animals, from insects to mice, whole. Owls then regurgitate pellets full of indigestible elements of their meal like animal bones and fur.',
              style: TextStyle(fontSize: 18),
            ),
          ),
        ],
      ),
    );
  }
}
