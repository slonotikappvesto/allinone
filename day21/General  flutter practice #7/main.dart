import 'package:dau21slider/smiley_controller.dart';
import 'package:dau21slider/wave_slider.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      home: App(),
    ));

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  int _rating = 50;
  int _ratingLast = 5;
  String _raitingWow = '';
  String _currentAnimation = '5+';
  SmileyController _smileyController = SmileyController();

  _changeSmile(int smile) {
    if (_ratingLast == smile) return;
    var direction = smile < _ratingLast ? '-' : '+';
    _currentAnimation = '$smile$direction';
    _ratingLast = smile;
  }

  @override
  Widget build(BuildContext context) {
    if (_rating <= 20) {
      {
        _raitingWow = 'Почему он еще работает в AppVesto?';
        _changeSmile(1);
      }
    } else if (_rating > 20 && _rating <= 50) {
      _raitingWow = 'Такое себе :(';
      _changeSmile(2);
    } else if (_rating > 50 && _rating <= 70) {
      _raitingWow = 'Идеальных не бывает, смирись.';
      _changeSmile(3);
    } else if (_rating > 70 && _rating <= 80)
      _raitingWow = 'Уго, прикольный у тебя ментор.';
    else if (_rating > 80 && _rating <= 90)
      _raitingWow = 'Да он хАрош!';
    else if (_rating > 90 && _rating <= 99) {
      _raitingWow = 'Он ГЕНИЙ!!!';
      _changeSmile(4);
    } else if (_rating == 100) {
      _raitingWow = 'Я тебе не верю, не обмануй!';
      _currentAnimation = '5+';
      _ratingLast = 5;
    }

    //_changeSmile();
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(32.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 300,
                width: 300,
                child: FlareActor(
                  'rive/data/happiness_emoji.flr',
                  alignment: Alignment.center,
                  fit: BoxFit.contain,
                  controller: _smileyController,
                  animation: _currentAnimation,
                ),
              ),
              Text(
                'На сколько крут ваш ментор?',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 32,
                ),
              ),
              WaveSlider(
                onChanged: (double val) {
                  setState(() {
                    _rating = (val * 100).round();
                  });
                },
              ),
              SizedBox(
                height: 50.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 20.0),
                  Text(
                    _rating.toString(),
                    style: TextStyle(fontSize: 45.0),
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Text(
                    '%',
                    style: TextStyle(fontSize: 46),
                  ),
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                '$_raitingWow',
                style: TextStyle(fontSize: 16),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
