import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:reduxsimple/redux/app_state.dart';

import 'redux/actions.dart';
import 'redux/reducers.dart';

void main() {
  final Store<AppState> store =
      Store(reducer, initialState: AppState(counter: 0, text: "Waiting..."));
  runApp(StoreProvider(
    store: store,
    child: MaterialApp(
      home: _Counter(),
    ),
  ));
}

class _Counter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Store<AppState> store = StoreProvider.of<AppState>(context);
    String inputText = "";
    return Scaffold(
      appBar: AppBar(
        title: Text("Redux"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => store.dispatch(AddAction()),
        child: Icon(
          Icons.fire_hydrant,
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 200,
              child: TextField(
                onChanged: (value) => inputText = value,
              ),
            ),
            SizedBox(height: 20),
            TextButton(
                onPressed: () => store.dispatch(SetTextAction(text: inputText)),
                child: Text("Press to input data")),
            StoreConnector<AppState, AppState>(
                converter: (store) => store.state,
                builder: (context, vm) => Text(vm.text)),
            SizedBox(height: 120),
            StoreConnector<AppState, AppState>(
              converter: (store) => store.state,
              builder: (context, state) => Text(
                state.counter.toString(),
                style: TextStyle(fontSize: 35),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
