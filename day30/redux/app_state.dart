import 'package:flutter/material.dart';

class AppState {
  final int counter;
  final String text;

  AppState({
    @required this.counter,
    @required this.text,
  });
}
