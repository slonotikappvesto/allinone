import 'package:anon_auth/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    name: 'SecondaryApp',
    options: const FirebaseOptions(
        appId:
            '266279856141-aajjc7uho00rr5dqeeprbekobr6l69st.apps.googleusercontent.com',
        apiKey: 'AIzaSyBUiSagyvWdoaIaQpN0fbXkoxuQ30flL5g',
        messagingSenderId: 'my_messagingSenderId',
        projectId: 'test-d936b'),
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Auth Anon',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _initialized = false;
  bool _error = false;

  Future<void> initializeFlutterFire() async {
    try {
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
    } catch (e) {
      setState(() {
        _error = true;
      });
    }
  }

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    if (_error) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Auth'),
        ),
        body: Center(
          child: Container(
            alignment: Alignment.center,
            height: 60,
            width: 150,
            color: Colors.redAccent,
            child: Text('Ошибка'),
          ),
        ),
      );
    }
    if (!_initialized) {
      return Scaffold(
        appBar: AppBar(
          title: Text('No Auth'),
        ),
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Auth'),
      ),
      body: Center(
        child: InkWell(
          onTap: () async {
            dynamic result = await _auth.signAnon();
            print(result);
          },
          child: Container(
            alignment: Alignment.center,
            height: 60,
            width: 150,
            color: Colors.greenAccent,
            child: Text('Auth'),
          ),
        ),
      ),
    );
  }
}
