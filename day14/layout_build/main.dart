import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Layout building practice',
      home: HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomePage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: [
          Positioned(
            top: -width * 0.5,
            right: -width * 0.3,
            child: Container(
              height: height * 0.8,
              width: width * 0.8,
              decoration: BoxDecoration(
                color: Color(0xFFe95d55),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: -height * 0.02,
            left: width * 0.35,
            child: Container(
              height: height * 0.6,
              width: width * 0.6,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xFFe95d55),
                  width: 2.0,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: height * 0.1,
            left: width * 0.85,
            child: Container(
              height: height * 0.3,
              width: width * 0.3,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xFFf4eadd),
                  width: 2.0,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: height * 0.6,
            left: -width * 0.25,
            child: Container(
              height: height * 0.7,
              width: width * 0.7,
              decoration: BoxDecoration(
                color: Color(0xFFe95d55),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: height * 0.75,
            left: width * 0.1,
            child: Container(
              height: height * 0.5,
              width: width * 0.5,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xFFe95d55),
                  width: 2.0,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: height * 0.65,
            left: -width * 0.1,
            child: Container(
              height: height * 0.3,
              width: width * 0.3,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xFFf4eadd),
                  width: 2.0,
                ),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Container(
              height: height * 0.08,
              width: width * 0.9,
              margin: EdgeInsets.only(
                  top: height * 0.05,
                  right: height * 0.04,
                  left: height * 0.04),
              decoration: BoxDecoration(
                border: Border.symmetric(
                  horizontal: BorderSide(
                    width: 2,
                  ),
                ),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Custom APP',
                      ),
                      IconButton(icon: Icon(Icons.menu), onPressed: () {})
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
