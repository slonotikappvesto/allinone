import 'package:flutter/material.dart';
import 'package:prac13/screens/first_screen.dart';
import 'package:prac13/services/router_generator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PracticAll",
      home: FirstScreen(),
      onGenerateRoute: RouteGenerater.generateRoute,
    );
  }
}
