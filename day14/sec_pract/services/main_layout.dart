import 'package:flutter/material.dart';
import 'package:prac13/widgets/main_drawer.dart';

class MainLayout extends StatelessWidget {
  final Widget child;
  final Widget drawer;

  MainLayout({this.child, this.drawer});

  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      drawer: MyDrawer(),
      appBar: AppBar(
        title: Text('Error'),
        actions: [
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                _key.currentState.openDrawer();
              })
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: child,
      ),
    );
  }
}
