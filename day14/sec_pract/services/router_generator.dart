import 'package:flutter/material.dart';
import 'package:prac13/screens/error_screen.dart';
import 'package:prac13/screens/first_screen.dart';
import 'package:prac13/screens/second_screen.dart';
import 'package:prac13/screens/wow_page.dart';

class RouteGenerater {
  static const ROUTE_FIRST = "/first";
  static const ROUTE_SECOND = "/second";
  static const ROUTE_WOW = "/wow";
  static const ROUTE_ERROR = "/error";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ROUTE_FIRST:
        final page = FirstScreen();
        return MaterialPageRoute(builder: (context) => page);

      case ROUTE_SECOND:
        final page = SecondtScreen();
        return MaterialPageRoute(builder: (context) => page);

      case ROUTE_WOW:
        final page = WowScreen();
        return MaterialPageRoute(builder: (context) => page);

      default:
        return MaterialPageRoute(builder: (context) => ErrorScreen());
    }
  }
}
