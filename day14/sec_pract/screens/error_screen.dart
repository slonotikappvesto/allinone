import 'package:flutter/material.dart';
import 'package:prac13/services/main_layout.dart';

class ErrorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('FASTER FASTER...'),
          ],
        ),
      ),
    );
  }
}
