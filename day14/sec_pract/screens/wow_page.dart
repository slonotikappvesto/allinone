import 'package:flutter/material.dart';
import 'package:prac13/services/main_layout.dart';

class WowScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Current page: Wow Page'),
            SizedBox(
              height: 50,
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).pushNamed('/error');
              },
              child: Container(
                alignment: Alignment.center,
                color: Colors.redAccent,
                height: 70,
                width: 150,
                child: Text(
                  'Error Page',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
