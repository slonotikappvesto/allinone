import 'package:flutter/material.dart';
import 'package:prac13/services/router_generator.dart';

class MyDrawer extends StatelessWidget {
  Widget buildButton(String name, Function tapHandler) {
    return InkWell(
      onTap: tapHandler,
      child: Container(
        alignment: Alignment.center,
        height: 600,
        width: 250,
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 5,
        ),
        decoration: BoxDecoration(
          color: Colors.blueAccent,
        ),
        child: Text(
          name,
          style: TextStyle(
            color: Colors.white,
            fontSize: 22,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            buildButton('First', () {
              Navigator.of(context).pushNamed(RouteGenerater.ROUTE_FIRST);
            }),
            buildButton('Second', () {
              Navigator.of(context).pushNamed(RouteGenerater.ROUTE_SECOND);
            }),
            buildButton('Wow', () {
              Navigator.of(context).pushNamed(RouteGenerater.ROUTE_WOW);
            }),
            buildButton('Error', () {
              Navigator.of(context).pushNamed(RouteGenerater.ROUTE_ERROR);
            }),
          ],
        ),
      ),
    );
  }
}
