import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Wow",
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _titleController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final isLandscape = MediaQuery
        .of(context)
        .orientation == Orientation.landscape;
    return Scaffold(
      appBar: AppBar(
        title: Text('LP'),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: mediaQuery.size.height * 0.75,
          width: mediaQuery.size.width * 0.75,
          color: Colors.red,
          child: Text(isLandscape ? 'Landscape' : 'Portrait'),
        ),
      ),
    );
  }
}
