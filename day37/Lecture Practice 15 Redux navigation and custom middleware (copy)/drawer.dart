import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:wotw10/src/app_store.dart';
import 'package:wotw10/src/login/login_bloc.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, LoginBloc>(converter: (store) {
      return LoginBloc(store);
    }, builder: (BuildContext context, LoginBloc bloc) {
      return Drawer(
        child: Padding(
          padding: const EdgeInsets.only(top: 58.0),
          child: Column(
            children: [
              RaisedButton(
                onPressed: () {
                  bloc.login();
                },
                child: Text('1'),
              ),
              RaisedButton(
                onPressed: () {
                  bloc.login();
                },
                child: Text('2'),
              ),
              RaisedButton(
                onPressed: () {
                  bloc.login();
                },
                child: Text('3'),
              ),
              RaisedButton(
                onPressed: () {
                  bloc.login();
                },
                child: Text('4'),
              ),
            ],
          ),
        ),
      );
    });
  }
}
