import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:wotw10/src/app_store.dart';

class LoginBloc {
  final Store<AppState> _store;

  LoginBloc(this._store);

  void login() {
    this._store.dispatch(NavigateToAction.replace("/dashboard"));
  }
}
