import 'package:flutter/material.dart';
import 'package:prac13/providers/counter.dart';
import 'package:prac13/screens/btn_screen.dart';
import 'package:prac13/screens/counter_screen.dart';
import 'package:prac13/widgets/myButton.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => Counter(),
      child: MaterialApp(
        title: 'Counter',
        home: HomePage(),
        routes: {
          ButtonScreen.routeName: (ctx) => ButtonScreen(),
          CounterScreen.routeName: (ctx) => CounterScreen(),
        },
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Counter'),
      ),
      body: Center(
        child: Container(
          width: double.infinity,
          height: 400,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Counter | faster faster...'),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyButton(() {
                    Navigator.of(context).pushNamed(CounterScreen.routeName);
                  }, 'Counter'),
                  SizedBox(
                    width: 20,
                  ),
                  MyButton(() {
                    Navigator.of(context).pushNamed(ButtonScreen.routeName);
                  }, 'Buttons'),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
