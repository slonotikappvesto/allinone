import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final Function handler;
  final String title;

  const MyButton(this.handler, this.title);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => handler(),
      splashColor: Colors.red,
      child: Container(
        alignment: Alignment.center,
        height: 50,
        width: 130,
        color: Colors.blueAccent,
        child: Text(title),
      ),
    );
  }
}
