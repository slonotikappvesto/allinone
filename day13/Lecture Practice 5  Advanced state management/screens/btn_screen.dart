import 'package:flutter/material.dart';
import 'package:prac13/providers/counter.dart';
import 'package:prac13/widgets/myButton.dart';
import 'package:provider/provider.dart';

class ButtonScreen extends StatelessWidget {
  static const routeName = '/button';
  @override
  Widget build(BuildContext context) {
    final counter = Provider.of<Counter>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Button'),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyButton(() {
              counter.decCounter();
            }, '-'),
            SizedBox(
              width: 20,
            ),
            MyButton(() {
              counter.incCounter();
            }, '+'),
          ],
        ),
      ),
    );
  }
}
