import 'package:flutter/material.dart';
import 'package:prac13/providers/counter.dart';
import 'package:provider/provider.dart';

class SnackBarMy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final counter = Provider.of<Counter>(context);
    return InkWell(
      onTap: () {
        Scaffold.of(context).hideCurrentSnackBar();
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: Text(
              counter.getCounter.toString(),
              textAlign: TextAlign.center,
            ),
            duration: Duration(milliseconds: 1200),
          ),
        );
      },
      child: Container(
        alignment: Alignment.center,
        height: 50,
        width: 130,
        color: Colors.orangeAccent,
        child: Text('SnackBar'),
      ),
    );
  }
}
