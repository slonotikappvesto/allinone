import 'package:flutter/material.dart';
import 'package:prac13/providers/counter.dart';
import 'package:provider/provider.dart';

class CounterScreen extends StatelessWidget {
  static const routeName = '/counter';
  @override
  Widget build(BuildContext context) {
    final counter = Provider.of<Counter>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Couner'),
      ),
      body: Center(
        child: Text(
          counter.getCounter.toString(),
        ),
      ),
    );
  }
}
