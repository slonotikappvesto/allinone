import 'package:flutter/cupertino.dart';

class Counter with ChangeNotifier {
  var _counter = 0;

  int get getCounter {
    return _counter;
  }

  void incCounter() {
    _counter++;
    notifyListeners();
  }

  void decCounter() {
    _counter--;
    notifyListeners();
  }
}
